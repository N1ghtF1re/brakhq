package men.brakh.brakhq.model.urlgenerator;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class ShiftBasedUrlGeneratorTest {




    @Test
    public void generate() {
        List<String> hashes = new ArrayList<>();
        Set<String> hashesSet = new HashSet<>();
        ShiftBasedUrlGenerator urlGenerator = new ShiftBasedUrlGenerator();

        for(int i = 1; i < 900000; i++) {
            String hash = urlGenerator.generate(i);
            hashes.add(hash);
            hashesSet.add(hash);
        }

        assertEquals(hashes.size(), hashesSet.size());
    }
}