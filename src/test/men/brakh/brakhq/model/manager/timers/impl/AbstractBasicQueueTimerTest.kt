package men.brakh.brakhq.model.manager.timers.impl

import men.brakh.brakhq.model.entity.Queue
import men.brakh.brakhq.service.queuetimers.timers.impl.AbstractBasicQueueTimer
import org.junit.Test
import java.sql.Timestamp
import java.util.*

class AbstractBasicQueueTimerTest {

    internal class TestQueueTimer(override val action: (queue: Queue) -> Unit): AbstractBasicQueueTimer(action) {
        override fun Queue.getTimerDate(): Date = this.regStartDate

        override fun isNeedToAdd(queue: Queue): Boolean = true
    }

    private fun queue(afterSeconds: Int, name: String = ""): Queue {
        val cal = Calendar.getInstance()
        cal.time = Date()
        cal.add(Calendar.SECOND, afterSeconds)

        val queue = Queue()
        queue.regStartDate = Timestamp(cal.time.time)
        queue.name = name
        return queue
    }

    @Test
    fun test1() {
        val queue = Queue()
        queue.regStartDate = Timestamp(Date().time)

        val timer = TestQueueTimer {
            assert(true)
        }

        timer.add(queue)
    }

    @Test
    fun test2() {
        val queuesQueue = listOf(
                queue(3, "3"), queue(2, "2"), queue(1, "1")
        )

        var number = 1

        val timer = TestQueueTimer {
            assert(it.name == queuesQueue[queuesQueue.size - number++].name) {
                "Error. Queue ${it.name} ended ${number - 1}th"
            }
        }

        queuesQueue.forEach(timer::add)

        Thread.sleep(3100)
        assert(number == 4)
    }

    @Test
    fun test3() {
        val queuesQueue = listOf(
                queue(1, "1"), queue(2, "2"), queue(3, "3")
        )

        var number = 0

        val timer = TestQueueTimer {
            assert(it.name == queuesQueue[number++].name) {
                "Error. Queue ${it.name} ended ${number - 1}th"
            }
        }

        queuesQueue.forEach(timer::add)

        Thread.sleep(3100)
        assert(number == 3)
    }

}