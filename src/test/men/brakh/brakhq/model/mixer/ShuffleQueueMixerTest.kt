package men.brakh.brakhq.model.mixer

import men.brakh.brakhq.model.entity.Place
import men.brakh.brakhq.model.entity.Queue
import men.brakh.brakhq.model.entity.User
import men.brakh.brakhq.model.enums.QueueTypes
import org.junit.Test
import kotlin.test.assertNull

class ShuffleQueueMixerTest {
    private val mixer = ShuffleQueueMixer()

    private fun generateQueue(places: Int, type: QueueTypes = QueueTypes.Random): Queue {
        val queue = Queue()
        queue.type = type
        queue.busyPlaces = generateSequence(0) {it + 1}
                .take(places)
                .map { Place(
                        user = User(name = "User$it", username = "User$it", email = "", password = ""),
                        place = it,
                        queue = queue
                    )
                    }
                .toList()
                .shuffled()
        return queue
    }


    @Test
    fun mixTest1() {
        val queue1 = generateQueue(15)

        val mixedQueue = mixer.mix(queue1)!!
        assert(mixedQueue.busyPlaces.map{ it.place }.sorted() == (1..queue1.busyPlaces.size).toList())
    }

    @Test
    fun mixTest2() {
        val queue1 = generateQueue(2)

        val mixedQueue = mixer.mix(queue1)!!
        assert(mixedQueue.busyPlaces.map{ it.place }.sorted() == (1..queue1.busyPlaces.size).toList())
    }

    @Test
    fun mixTest3() {
        val queue1 = generateQueue(1)

        val mixedQueue = mixer.mix(queue1)!!
        assert(mixedQueue.busyPlaces.map{ it.place }.sorted() == (1..queue1.busyPlaces.size).toList())
    }

    @Test
    fun mixTest4() {
        val queue1 = generateQueue(0)

        val mixedQueue = mixer.mix(queue1)!!
        assert(mixedQueue.busyPlaces.size == 0)
    }

    @Test
    fun mixTest5() {
        val queue1 = generateQueue(0, QueueTypes.Default)

        val mixedQueue = mixer.mix(queue1)

        assertNull(mixedQueue)
    }

    @Test
    fun mixTest6() {
        val queue1 = generateQueue(2)
        queue1.isMixed = true

        val mixedQueue = mixer.mix(queue1)

        assertNull(mixedQueue)
    }



}