package men.brakh.brakhq.extensions

import org.junit.Test
import java.text.SimpleDateFormat
import java.util.*

class DateComparatorTest {
    private val dateTimeFormat = SimpleDateFormat("dd.MM.yyy HH:mm")
    private fun String.toDate(): Date = dateTimeFormat.parse(this)

    @Test
    fun test1() = assert("01.02.2019 16:41".toDate() >= "01.02.2019 16:41".toDate())

    @Test
    fun test2() = assert("01.02.2019 16:41".toDate() == "01.02.2019 16:41".toDate())

    @Test
    fun test3() = assert("01.02.2019 16:41".toDate() <= "01.02.2019 16:41".toDate())

    @Test
    fun test4() = assert("01.02.2019 16:42".toDate() >= "01.02.2019 16:41".toDate())

    @Test
    fun test5() = assert("01.02.2019 16:42".toDate() > "01.02.2019 16:41".toDate())

    @Test
    fun test6() = assert("01.02.2019 16:41".toDate() < "01.02.2019 16:45".toDate())
}