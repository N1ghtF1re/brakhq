package men.brakh.brakhq.extensions

import org.junit.Test
import java.text.SimpleDateFormat
import java.util.*
import kotlin.test.assertFalse

class DateRangeTest {
    private val format = SimpleDateFormat("dd.MM.yyyy")
    private val dateTimeFormat = SimpleDateFormat("dd.MM.yyy HH:mm")
    private fun String.toDate(): Date = format.parse(this)
    private fun String.toDateTime(): Date = dateTimeFormat.parse(this)

    @Test
    fun test1() = assert( "03.01.2017".toDate() in "01.01.2017".toDate().."04.01.2017".toDate() )

    @Test
    fun test2() = assert( "03.01.2017".toDate() in "03.01.2017".toDate().."04.01.2017".toDate() )

    @Test
    fun test3() = assert( "03.01.2017".toDate() in "01.01.2017".toDate().."03.01.2017".toDate() )

    @Test
    fun test4() = assert( "03.01.2017".toDate() in "03.01.2017".toDate().."03.01.2017".toDate() )

    @Test
    fun test5() = assertFalse ( "06.01.2017".toDate() in "01.01.2017".toDate().."05.01.2017".toDate() )

    @Test
    fun test6() = assert ( "06.01.2017 15:43".toDateTime() in
            "06.01.2017 15:42".toDateTime().."06.01.2017 15:44".toDateTime() )




}