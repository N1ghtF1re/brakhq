package men.brakh.brakhq.validator

import men.brakh.brakhq.localisation.Translator
import men.brakh.brakhq.model.entity.Field
import men.brakh.brakhq.model.entity.Place
import men.brakh.brakhq.model.entity.Queue
import men.brakh.brakhq.model.entity.User
import men.brakh.brakhq.model.enums.QueueTypes
import men.brakh.brakhq.service.db.UserService
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import java.sql.Timestamp
import kotlin.test.assertEquals
import kotlin.test.assertNull

class QueueValidatorTest {
    private val userService = mock(UserService::class.java)
    private val queueValidator = QueueValidator(userService)
    private val testUser = User(username = "TestUser", password = "Qwerty", email = "test@brakh.men", name = "Test")

    @Before
    fun initTestContext() {
        SecurityContextHolder.getContext().authentication = UsernamePasswordAuthenticationToken(
                testUser, null, emptyList<GrantedAuthority>())


        Mockito.doReturn(testUser)
                .`when`(userService)
                .findByUsernameOrNull(anyString())
    }


    @Test
    fun validateCreating_NoQueueName() {
        val newQueue = Queue(testUser)
        val expectedMsg = Translator.toLocale("paramNameRequired")

        newQueue.name = null

        assertEquals(expectedMsg, queueValidator.validateCreating(newQueue))
    }

    @Test
    fun validateCreating_EmptyQueueName_1() {
        val newQueue = Queue(testUser)
        val expectedMsg = Translator.toLocale("paramNameCantBeEmpty")

        newQueue.name = ""

        assertEquals(expectedMsg, queueValidator.validateCreating(newQueue))
    }

    @Test
    fun validateCreating_EmptyQueueName_2() {
        val newQueue = Queue(testUser)
        val expectedMsg = Translator.toLocale("paramNameCantBeEmpty")

        newQueue.name = "   "

        assertEquals(expectedMsg, queueValidator.validateCreating(newQueue))
    }

    @Test
    fun validateCreating_NoQueueType() {
        val newQueue = Queue(testUser)
        val expectedMsg = Translator.toLocale("paramQueueTypeRequired")

        newQueue.apply {
            name = "TestQueue"
            type = null
        }

        assertEquals(expectedMsg, queueValidator.validateCreating(newQueue))
    }

    @Test
    fun validateCreating_NoRegStartDate() {
        val newQueue = Queue(testUser)
        val expectedMsg = Translator.toLocale("paramRegStartRequired")

        newQueue.apply {
            name = "TestQueue"
            type = QueueTypes.Default
            regStartDate = null
        }

        assertEquals(expectedMsg, queueValidator.validateCreating(newQueue))
    }

    @Test
    fun validateCreating_NoEventDate() {
        val newQueue = Queue(testUser)
        val expectedMsg = Translator.toLocale("paramEventDateRequired")

        newQueue.apply {
            name = "TestQueue"
            type = QueueTypes.Default
            regStartDate = Timestamp(1560729600)
            eventDate = null
        }

        assertEquals(expectedMsg, queueValidator.validateCreating(newQueue))
    }

    @Test
    fun validateCreating_NoRegEndDate() {
        val newQueue = Queue(testUser)
        val expectedMsg = Translator.toLocale("paramRegEndRequired")

        newQueue.apply {
            name = "TestQueue"
            type = QueueTypes.Default
            regStartDate = Timestamp(1560729600)
            eventDate = Timestamp(1560729800)
            regEndDate = null
        }

        assertEquals(expectedMsg, queueValidator.validateCreating(newQueue))
    }

    @Test
    fun validateCreating_NoPlaces_1() {
        val newQueue = Queue(testUser)
        val expectedMsg = Translator.toLocale("queueNoPlaces")

        newQueue.apply {
            name = "TestQueue"
            type = QueueTypes.Default
            regStartDate = Timestamp(1560729600)
            eventDate = Timestamp(1560729800)
            regEndDate = Timestamp(1560729700)
            placesCount = 0
        }

        assertEquals(expectedMsg, queueValidator.validateCreating(newQueue))
    }

    @Test
    fun validateCreating_NoPlaces_2() {
        val newQueue = Queue(testUser)
        val expectedMsg = Translator.toLocale("queueNoPlaces")

        newQueue.apply {
            name = "TestQueue"
            type = QueueTypes.Default
            regStartDate = Timestamp(1560729600)
            eventDate = Timestamp(1560729800)
            regEndDate = Timestamp(1560729700)
            placesCount = -5
        }

        assertEquals(expectedMsg, queueValidator.validateCreating(newQueue))
    }

    @Test
    fun validateCreating_Successful() {
        val newQueue = Queue(testUser)

        newQueue.apply {
            name = "TestQueue"
            type = QueueTypes.Default
            regStartDate = Timestamp(1560729600)
            eventDate = Timestamp(1560729800)
            regEndDate = Timestamp(1560729700)
            placesCount = 5
        }

        assertNull(queueValidator.validateCreating(newQueue))
        Mockito.verify(userService, Mockito.times(1)).findByUsernameOrNull(anyString())
    }


    @Test
    fun validateChanging_editUrl() {
        val editedQueue = Queue()
        val expectedMsg = Translator.toLocale("userCantChangeQueueUrl")

        editedQueue.url = "Test"

        assertEquals(expectedMsg, queueValidator.validateChanging(editedQueue))
    }

    @Test
    fun validateChanging_editType() {
        val editedQueue = Queue()
        val expectedMsg = Translator.toLocale("userCantChangeQueueType")

        editedQueue.type = QueueTypes.Default

        assertEquals(expectedMsg, queueValidator.validateChanging(editedQueue))
    }

    @Test
    fun validateChanging_editOwner() {
        val editedQueue = Queue()
        val expectedMsg = Translator.toLocale("userCantChangeQueueOwner")

        editedQueue.owner = testUser

        assertEquals(expectedMsg, queueValidator.validateChanging(editedQueue))
    }

    @Test
    fun validateChanging_editBusyPlaces() {
        val editedQueue = Queue()
        val expectedMsg = Translator.toLocale("userCantChangeBusyPlaces")

        editedQueue.busyPlaces = listOf(Place(testUser, editedQueue, 5))

        assertEquals(expectedMsg, queueValidator.validateChanging(editedQueue))
    }

    @Test
    fun validateChanging_editFields() {
        val editedQueue = Queue()
        val expectedMsg = Translator.toLocale("userCantChangeQueueFields")

        editedQueue.fields = listOf(Field())

        assertEquals(expectedMsg, queueValidator.validateChanging(editedQueue))
    }

    @Test
    fun validateChanging_editName_1() {
        val editedQueue = Queue()
        val expectedMsg = "paramNameCantBeEmpty"

        editedQueue.name = ""

        assertEquals(expectedMsg, queueValidator.validateChanging(editedQueue))
    }

    @Test
    fun validateChanging_editName_2() {
        val editedQueue = Queue()
        val expectedMsg = Translator.toLocale("paramNameCantBeEmpty")

        editedQueue.name = "   "

        assertEquals(expectedMsg, queueValidator.validateChanging(editedQueue))
    }

    @Test
    fun validateChanging_editPlacesCount() {
        val editedQueue = Queue()
        val expectedMsg = Translator.toLocale("getQueueNoPlaces")

        editedQueue.placesCount = -5

        assertEquals(expectedMsg, queueValidator.validateChanging(editedQueue))
    }

    @Test
    fun validateChanging_Successful() {
        val editedQueue = Queue()

        editedQueue.apply {
            name = "New name"
            placesCount = 10
        }

        assertNull(queueValidator.validateChanging(editedQueue))
    }
}