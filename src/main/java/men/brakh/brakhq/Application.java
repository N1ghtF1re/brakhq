package men.brakh.brakhq;

import men.brakh.brakhq.service.queuetimers.QueueTimersService;
import men.brakh.brakhq.service.storage.StorageService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;

import java.util.TimeZone;
import java.util.concurrent.Executors;

@SpringBootApplication
@EnableScheduling
public class Application{

    public static void main(String[] args) throws Exception {
        TimeZone.setDefault(TimeZone.getTimeZone(AppConfig.defaultTimeZone));
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner initStorage(StorageService storageService) {
        return (args) -> storageService.init();
    }

    @Bean
    CommandLineRunner initQueueTimersService(QueueTimersService queueTimersService) {
        return (args) -> queueTimersService.init();
    }

    @Bean
    public TaskScheduler taskScheduler() {
        return new ConcurrentTaskScheduler(Executors.newSingleThreadScheduledExecutor());
    }
}
