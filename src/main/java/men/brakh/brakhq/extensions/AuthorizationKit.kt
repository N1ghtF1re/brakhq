package men.brakh.brakhq.extensions

import men.brakh.brakhq.config.SpringContext
import men.brakh.brakhq.exceptions.UnauthorizedException
import men.brakh.brakhq.model.entity.User
import men.brakh.brakhq.service.db.UserService
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder

fun getAuthorizedUser(): User? {
    val auth: Authentication? = SecurityContextHolder.getContext().authentication
    val userService = SpringContext.getBean(UserService::class.java)

    return auth?.let { userService.findByUsernameOrNull(it.name) }
}

inline fun authorized(block: (User) -> Unit) {
    val authorizedUser: User = getAuthorizedUser() ?: throw UnauthorizedException("Unauthorized")
    block(authorizedUser)
}

inline fun ifAuthorized(block: (User) -> Unit): Boolean {
    val authorizedUser: User = getAuthorizedUser() ?: return false
    block(authorizedUser)
    return true
}