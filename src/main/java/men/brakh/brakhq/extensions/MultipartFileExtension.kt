package men.brakh.brakhq.extensions

import org.springframework.web.multipart.MultipartFile

fun MultipartFile.isImage(): Boolean = contentType?.startsWith("image") ?: false

val MultipartFile.extension: String
    get() = this.originalFilename!!.substring(this.originalFilename!!.lastIndexOf(".") + 1);


