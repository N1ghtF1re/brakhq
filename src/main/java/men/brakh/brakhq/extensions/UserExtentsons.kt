package men.brakh.brakhq.extensions

import men.brakh.brakhq.config.SpringContext
import men.brakh.brakhq.model.dto.UserDto
import men.brakh.brakhq.model.entity.Queue
import men.brakh.brakhq.model.entity.User
import men.brakh.brakhq.service.db.QueueService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

private val bCryptPasswordEncoder = SpringContext.getBean(BCryptPasswordEncoder::class.java)
private val queueService = SpringContext.getBean(QueueService::class.java)

val User.usedQueues: List<Queue>
    get() = this.places.map { place -> place.queue }

val User.subscribedQueues: List<Queue>
    get() = this.subscriptions.map { it.queue }

val User.createdQueues: List<Queue>
    get() = queueService.findByOwner(id).toList()

fun User.toDto(): UserDto = UserDto(id = this.id, name = this.name, avatar = this.getAvatar())

fun User.toExtendedDto(): UserDto = UserDto(id = this.id, name = this.name, avatar = this.getAvatar(),
            email = this.email, username = this.username)

fun User.editUser(changedUser: Map<String, String>): User {
    return this.copy(
            name = changedUser["name"] ?: this.name,
            avatarUrl = changedUser["avatar"] ?: this.avatarUrl,
            email = changedUser["email"] ?: this.email,
            password = changedUser["password"]?.let { bCryptPasswordEncoder.encode(it) } ?: this.password
    )
}