package men.brakh.brakhq.extensions

import java.util.*
import kotlin.math.sign

class DateRange(override val start: Date, override val endInclusive: Date): ClosedRange<Date>  {
    override fun isEmpty(): Boolean = start.time > endInclusive.time

    override operator fun contains(value: Date): Boolean
            = start.time <= value.time && value.time <= endInclusive.time

    override fun equals(other: Any?): Boolean =
            other is DateRange && (isEmpty() && other.isEmpty() ||
                    start.time == other.start.time && endInclusive.time == other.endInclusive.time)

    override fun hashCode(): Int {
        var result = start.hashCode()
        result = 31 * result + endInclusive.hashCode()
        return result
    }
}

fun Date.tomorrow(): Date {
    val cal = Calendar.getInstance()
    cal.time = this
    cal.add(Calendar.DATE, + 1)
    cal.set(Calendar.HOUR_OF_DAY, 0)
    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)

    return cal.time
}

operator fun Date.compareTo(date2: Date): Int {
    return (this.time - date2.time).sign
}

