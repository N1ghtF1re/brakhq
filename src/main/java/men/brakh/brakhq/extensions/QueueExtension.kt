package men.brakh.brakhq.extensions

import men.brakh.brakhq.config.SpringContext
import men.brakh.brakhq.exceptions.QueueIsFullException
import men.brakh.brakhq.model.entity.Place
import men.brakh.brakhq.model.entity.Queue
import men.brakh.brakhq.model.entity.User
import men.brakh.brakhq.model.enums.QueueEvent
import men.brakh.brakhq.model.enums.QueueTypes
import men.brakh.brakhq.model.mixer.QueueMixer
import men.brakh.brakhq.service.db.PlaceService
import men.brakh.brakhq.service.db.QueueService
import men.brakh.brakhq.service.observers.SubscriptionService

private val queueService = SpringContext.getBean(QueueService::class.java)
private val placeService = SpringContext.getBean(PlaceService::class.java)
private val queueMixer = SpringContext.getBean(QueueMixer::class.java)
private val subscriptionService = SpringContext.getBean(SubscriptionService::class.java)

fun Queue.isPlaceUnBusy(placeNumber: Int): Boolean = this.busyPlaces.none { place: Place -> place.place == placeNumber }
val Queue.members: List<User>
    get() = this.busyPlaces.map { it.user }
val Queue.subscribers: List<User>
    get() = subscriptionService.findByQueue(this).map { it.user }

fun Queue.findFirstUnBusyPlace(): Int {
    val placesCount = this.placesCount

    for (placeNumber in 1..placesCount) {
        if (this.isPlaceUnBusy(placeNumber))
            return placeNumber
    }

    throw QueueIsFullException()
}

fun Queue.editQueue(changedQueue: Queue) {
    val newName = changedQueue.name

    this.name = newName ?: name
    this.eventDate = changedQueue.eventDate ?: eventDate
    this.description = changedQueue.description ?: description

    queueService.update(this)
    changedQueue.notify(QueueEvent.QUEUE_CHANGE, this)
}

fun Queue.mix() {
    val queue = this
    if (queue.type == QueueTypes.Random) {
        val dbQueue = queueService.findById(queue.id!!)

        val mixedQueue = queueMixer.mix(dbQueue) ?: return
        mixedQueue.isMixed = true

        mixedQueue.busyPlaces.forEach { placeService.save(it) }
        queueService.save(mixedQueue)
        mixedQueue.notify(QueueEvent.QUEUE_MIX, dbQueue)
    }
}
