package men.brakh.brakhq.validator;

import men.brakh.brakhq.localisation.TranslatorKt;
import men.brakh.brakhq.model.entity.Queue;
import men.brakh.brakhq.model.entity.User;
import men.brakh.brakhq.model.enums.QueueTypes;
import men.brakh.brakhq.service.db.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class QueueValidator {
    private final UserService userService;


    public QueueValidator(UserService userService) {
        this.userService = userService;
    }


    public String validateCreating(Queue queue) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) {
            return TranslatorKt.localeString("userNotAuth");
        }

        User currentUser = userService.findByUsernameOrNull(auth.getName());

        if (currentUser == null) {
            return TranslatorKt.localeString("userNotExist");
        }


        String newQueueName = queue.getName();
        if (newQueueName == null) {
            return TranslatorKt.localeString("paramNameRequired");
        }

        newQueueName = newQueueName.trim();

        if (newQueueName.length() == 0) {
            return TranslatorKt.localeString("paramNameCantBeEmpty");
        }


        QueueTypes newQueueType = queue.getType();
        if (newQueueType == null) {
            return TranslatorKt.localeString("paramQueueTypeRequired");
        }


        Timestamp regStartDate = queue.getRegStartDate();
        if (regStartDate == null) {
            return TranslatorKt.localeString("paramRegStartRequired");
        }

        Timestamp eventDate = queue.getEventDate();
        if (eventDate == null) {
            return TranslatorKt.localeString("paramEventDateRequired");
        }

        Timestamp regEndDate = queue.getRegEndDate();
        if (regEndDate == null) {
            return TranslatorKt.localeString("paramRegEndRequired");
        }


        int placesCount = queue.getPlacesCount();
        if (placesCount <= 1) {
            return TranslatorKt.localeString("queueNoPlaces");
        }

        Long newQueueId = queue.getId();
        if (newQueueId != null) {
            return TranslatorKt.localeString("userCantSetQueueId");
        }

        return null;
    }

    public String validateChanging(Queue queue) {
        if (queue.getUrl() != null) {
            return TranslatorKt.localeString("userCantChangeQueueUrl");
        }

        if (queue.getType() != null) {
            return TranslatorKt.localeString("userCantChangeQueueType");
        }

        if (queue.getOwner() != null) {
            return TranslatorKt.localeString("userCantChangeQueueOwner");
        }

        if (!queue.getBusyPlaces().isEmpty()) {
            return TranslatorKt.localeString("userCantChangeBusyPlaces");
        }

        if (!queue.getFields().isEmpty()) {
            return TranslatorKt.localeString("userCantChangeQueueFields");
        }

        String name = queue.getName();
        if (name != null && name.trim().length() == 0) {
            return TranslatorKt.localeString("paramNameCantBeEmpty");
        }

        if (queue.getPlacesCount() < 2) {
            return TranslatorKt.localeString("getQueueNoPlaces");
        }

        return null;
    }
}
