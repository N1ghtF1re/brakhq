package men.brakh.brakhq.validator;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import men.brakh.brakhq.localisation.TranslatorKt;
import men.brakh.brakhq.model.entity.User;
import men.brakh.brakhq.security.config.ValidationProperties;
import men.brakh.brakhq.service.db.UserService;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
/**
 * ВАЛИДАЦИЯ ПОЛЕЙ ПОЛЬЗОВАТЕЛЯ ПРИ РЕГИСТРАЦИИ
 */
public class UserValidator {
    private final UserService userService;

    private final ValidationProperties validationProperties;


    private final ObjectMapper mapper;

    public UserValidator(UserService userService, ValidationProperties validationProperties, ObjectMapper mapper) {
        this.userService = userService;
        this.validationProperties = validationProperties;
        this.mapper = mapper;
    }


    public Map<String, String> validate(Map<String, String> user) {
        Map<String, String> errors = new HashMap<>();

        errors.putAll(validateUsername(user));
        errors.putAll(validatePassword(user));
        errors.putAll(validateEmail(user));
        validateName(user);

        return errors;
    }

    private void validateName(Map<String, String> user) {
        if(!user.containsKey("name") && user.containsKey("username")) {
            user.put("name", user.get("username"));
        }
    }

    private Map<String, String>  validateUsername(Map<String, String> user) {
        Map<String, String>  usernameErrors = new HashMap<>();

        if(!user.containsKey("username")) {
            usernameErrors.put("username", validationProperties.getUsernameEmptyMsg());
        } else {
            // Удаляем пробелы в логине
            user.put("username", user.get("username").replaceAll(" ", ""));


            if (user.get("username").length() < validationProperties.getUsernameMinSize()
                    || user.get("username").length() > validationProperties.getUsernameMaxSize()) {
                usernameErrors.put("username", validationProperties.getUsernameSizeMsg());
            }

            if (userService.findByUsernameOrNull(user.get("username")) != null) {
                usernameErrors.put("username", validationProperties.getUsernameDuplicateMsg());
            }

        }

        return usernameErrors;
    }

    private Map<String, String> validatePassword(Map<String, String> user) {
        Map<String, String> passwordErrors = new HashMap<>();

        if(!user.containsKey("password")) {
            passwordErrors.put("password", validationProperties.getPasswordEmptyMsg());
        } else {
            // Удаляем пробелы в пароле
            user.put("password", user.get("password").replaceAll(" ", ""));

            if (user.get("password").length() < validationProperties.getPasswordMinSize()) {
                passwordErrors.put("password", validationProperties.getPasswordSizeMsg());
            }
        }

        return passwordErrors;
    }

    private Map<String, String> validateEmail(Map<String, String> user) {
        Map<String, String>  emailErrors = new HashMap<>();

        if(!user.containsKey("email")) {
            emailErrors.put("email", validationProperties.getEmailEmptyMsg());
        } else  {
            user.put("email", user.get("email").replaceAll(" ", ""));

            Pattern p = Pattern.compile(".+@.+\\..+"); // Эмейл должен содержать какие-то символы, после которых идет @,
            // потом опять символы, потом точка и опять символы
            Matcher m = p.matcher(user.get("email"));
            if(!m.matches()) {
                emailErrors.put("email", validationProperties.getEmailInvalidMsg());
            }

            User userWithEmail = userService.findByEmailOrNull(user.get("email"));
            if (userWithEmail != null ) {
                emailErrors.put("email", validationProperties.getEmailExistMsg());
            }
        }

        return emailErrors;
    }

    public Map<String, String> validateChanging(Map<String, String> user) {
        Map<String, String> errors = new HashMap<>();

        if (user.containsKey("name")) {
            String name = user.get("name");
            if (name.length() == 0) {
                errors.put("name", TranslatorKt.localeString("paramNameCantBeEmpty"));
            }
        }

        if (user.containsKey("password")) {
            errors.putAll(validatePassword(user));
        }

        if (user.containsKey("email")) {
            if(validateEmail(user).size() != 0) {
                user.remove("email");
            }
        }

        return errors;
    }

    public ObjectNode checkErrors(Map<String, String> errors) {
        ObjectNode errorNode = mapper.createObjectNode();
        if (!errors.isEmpty()) {
            ObjectNode errorsArrayNode = mapper.valueToTree(errors);

            errorNode.put("success", false);
            errorNode.put("errors", errorsArrayNode);
        }

        return errorNode;
    }
}
