package men.brakh.brakhq.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Deprecated
public class ModelResponse<T> {
    @JsonProperty("success")
    private boolean success;

    @JsonProperty("response")
    private T response;

    @JsonProperty("message")
    @JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
    private String error;

    public ModelResponse(String error) {
        this.success = false;
        this.error = error;
        this.response = null;
    }

    public ModelResponse(T response) {
        this.success = true;
        this.response = response;
        this.error = null;
    }


    public boolean isSuccess() {
        return success;
    }

    public T getResponse() {
        return response;
    }
}
