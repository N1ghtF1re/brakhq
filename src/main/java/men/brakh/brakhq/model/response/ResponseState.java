package men.brakh.brakhq.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Class for creating response.
 */
@Deprecated
public class ResponseState {
    @JsonProperty("success")
    private boolean ok;

    @JsonProperty("message")
    private String message;

    public ResponseState() {

    }

    /**
     * Constructor of class
     * @param ok - request status (no execution errors - true | error - false)
     * @param message - request status info (if no errors - "Success", else - error description)
     */
    public ResponseState(boolean ok, String message) {
        this.ok = ok;
        this.message = message;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
