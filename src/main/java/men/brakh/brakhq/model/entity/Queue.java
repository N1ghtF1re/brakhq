package men.brakh.brakhq.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import men.brakh.brakhq.model.enums.QueueTypes;
import men.brakh.brakhq.model.manager.QueueEventManager;
import men.brakh.brakhq.model.observer.Observable;
import men.brakh.brakhq.model.observer.Observer;
import men.brakh.brakhq.model.serializer.TimestampDeserializer;
import men.brakh.brakhq.model.serializer.TimestampSerializer;
import men.brakh.brakhq.model.urlgenerator.UrlGenerator;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.*;

@Entity
@Table(name="queues")
public class Queue implements Observable {
    @Transient
    private static QueueEventManager eventManager = QueueEventManager.INSTANCE;

    @JsonProperty("id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @JsonProperty("url")
    @Column(name = "url")
    private String url; // Адрес очереди

    @JsonProperty("name")
    @Column(name = "name")
    private String name; // Название очереди

    @JsonProperty("description")
    @Column(name="description")
    private String description; // Описание очереди

    @JsonProperty("queue_type")
    @Column(name="queue_type")
    @Enumerated(EnumType.STRING)
    private QueueTypes type; // Тип очереди

    @JsonProperty("owner")
    @ManyToOne // Один пользователь может владеть не сколькими очередями
    @JoinColumn(name = "user_id")
    private User owner;

    @JsonProperty("reg_start")
    @Column(name = "reg_start")
    @JsonSerialize(using = TimestampSerializer.class)
    @JsonDeserialize(using = TimestampDeserializer.class)
    private java.sql.Timestamp regStartDate; // Дата начала регистрации

    @JsonProperty("event_date")
    @Column(name = "event_date")
    @JsonDeserialize(using = TimestampDeserializer.class)
    @JsonSerialize(using = TimestampSerializer.class)
    private java.sql.Timestamp eventDate; // Дата начала события

    @JsonProperty("reg_end")
    @Column(name = "reg_end")
    @JsonDeserialize(using = TimestampDeserializer.class)
    @JsonSerialize(using = TimestampSerializer.class)
    private java.sql.Timestamp regEndDate; // Дата конца регистрации

    @JsonProperty("places_count")
    @Column(name = "places_count")
    private int placesCount; // Максимальное количество мест

    @Column(name = "mixed", columnDefinition = "boolean default false", nullable = false)
    @JsonIgnore
    private boolean isMixed = false; // перемешана ли очередь - для рандомных очередей.

    @JsonProperty("busy_places")
    @OneToMany(
            mappedBy = "queue",
            fetch=FetchType.EAGER,
            targetEntity = Place.class,
            orphanRemoval = true
    )
    private List<Place> busyPlaces = Collections.synchronizedList(new ArrayList<Place>()); // Занятые места

    // Поля будем выводить позже
    @JsonIgnore
    @OneToMany(
            mappedBy = "queue",
            fetch = FetchType.LAZY,
            targetEntity = Field.class,
            orphanRemoval = true
    )
    private List<Field> fields = new ArrayList<>();

    public Queue() {
        placesCount = 0;
    }

    public Queue(User owner) {
        this.owner = owner;
    }


    /**
     * Return true if registration started
     * @return true if registration started
     */
    public boolean isRegStarted() {
        long currentTime = new Date().getTime();
        return currentTime > regStartDate.getTime();
    }

    /**
     * Return true if registration ended
     * @return true if registration ended
     */
    public boolean isRegEnded() {
        long currentTime = new Date().getTime();
        return currentTime > regEndDate.getTime();
    }

    /**
     * Return true if registration started and not ended
     * @return true if registration started and not ended
     */
    public boolean isRegActive() {
        return isRegStarted() && !isRegEnded();
    }


    /**
     * Return true if the queue is full (busy places count == maximum number of places to queue)
     * @return true if the queue is full
     */
    @JsonIgnore
    public boolean isFull() {
        return getBusyPlaces().size() >= getPlacesCount();
    }

    @JsonIgnore
    public boolean isEmpty() {
        return getBusyPlaces().size() == 0;
    }

    /*
     * GETTERS AND SETTERS
     */

    public static QueueEventManager getEventManager() {
        return eventManager;
    }

    public static void setEventManager(QueueEventManager eventManager) {
        Queue.eventManager = eventManager;
    }

    public void setBudyPlaces(List<Place> places) {
        this.busyPlaces = places;
    }

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public Queue setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getName() {
        return name;
    }

    public Queue setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Queue setDescription(String description) {
        this.description = description;
        return this;
    }

    public void generateUrl(UrlGenerator urlGenerator) {
        url = urlGenerator.generate(id);
    }

    public QueueTypes getType() {
        return type;
    }

    public void setType(QueueTypes type) {
        this.type = type;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Timestamp getRegStartDate() {
        return regStartDate;
    }

    public void setRegStartDate(Timestamp regStartDate) {
        this.regStartDate = regStartDate;
    }

    public Timestamp getEventDate() {
        return eventDate;
    }

    public void setEventDate(Timestamp eventDate) {
        this.eventDate = eventDate;
    }

    public Timestamp getRegEndDate() {
        return regEndDate;
    }

    public void setRegEndDate(Timestamp regEndDate) {
        this.regEndDate = regEndDate;
    }

    public List<Place> getBusyPlaces() {
        return busyPlaces;
    }

    public void setBusyPlaces(List<Place> busyPlaces) {
        this.busyPlaces = busyPlaces;
    }

    public int getPlacesCount() {
        return placesCount;
    }

    public void setPlacesCount(int placesCount) {
        this.placesCount = placesCount;
    }

    @JsonIgnore
    public boolean isMixed() {
        return isMixed;
    }

    public void setMixed(boolean mixed) {
        isMixed = mixed;
    }


    /**
     * ПОДПИСКИ
     */
    @Override
    public void observe(Observer observer) {
        eventManager.add(this, observer);
    }

    @Override
    public void unObserve(Observer observer) {
        eventManager.remove(this, observer);
    }

    @Override
    public void notify(Enum event) {
        Set<Observer> observers = eventManager.getObservers(this);
        if(observers == null) return;
        observers.forEach(observer -> observer.update(event));
    }

    @Override
    public void notify(Enum event, Object object) {
        Set<Observer> observers = eventManager.getObservers(this);
        if(observers == null) return;
        observers.forEach(observer -> observer.update(event, object));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Queue queue = (Queue) o;
        return Objects.equals(id, queue.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
