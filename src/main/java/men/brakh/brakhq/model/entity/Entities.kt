package men.brakh.brakhq.model.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import men.brakh.brakhq.model.enums.NotificationTokenType
import org.hibernate.annotations.ColumnDefault
import org.springframework.security.core.GrantedAuthority
import java.net.InetAddress
import javax.persistence.*

@Entity
@Table(name = "subscription", uniqueConstraints = [UniqueConstraint(columnNames = ["user_id", "queue_id"])])
data class Subscription(@Id
                        @GeneratedValue(strategy = GenerationType.IDENTITY)
                        val id: Long = -1,

                        @ManyToOne
                        @JoinColumn(name = "user_id")
                        val user: User,

                        @ManyToOne
                        @JoinColumn(name = "queue_id")
                        val queue: Queue
)

@Entity
@Table(name = "nofication_tokens")
data class NotificationToken(
        @ManyToOne
        @JoinColumn(name = "user_id")
        val user: User,

        @Enumerated(EnumType.STRING)
        val type: NotificationTokenType,

        @Column(unique = true)
        val token: String,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = -1
)

@Entity
@Table(name = "places", uniqueConstraints = [UniqueConstraint(columnNames = ["user_id", "queue_id"]),
                        UniqueConstraint(columnNames = ["place", "queue_id"])])
data class Place(
        @ManyToOne
        @JoinColumn(name = "user_id")
        val user: User,

        @JsonIgnore
        @ManyToOne val queue: Queue,

        @Column(name = "place")
        val place: Int,

        @JsonIgnore
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = -1
)


@Entity
data class Role(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "role_id")
        val id: Long? = -1,

        @Column(name = "name")
        val name: String,

        @ManyToMany(mappedBy = "roles")
        var users: MutableSet<User>
) : GrantedAuthority {
    override fun toString(): String = name
    override fun getAuthority(): String = name
}


@Entity
@Table(name = "users")
data class User(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long = -1,

        @JsonIgnore // Логин отображается только владельцу
        @Column(unique = true)
        val username: String,

        @JsonProperty("name")
        @Column(name = "name")
        val name: String,

        @JsonIgnore
        @Column(name = "avatar")
        val avatarUrl: String? = null,

        @JsonIgnore
        @Column(name = "password")
        val password: String,

        @JsonIgnore // Email отображается только владельцу
        @Column(name = "email", unique = true)
        val email: String,

        @JsonIgnore
        @Column(name = "user_source")
        @ColumnDefault("'site'")
        val from: String = "site"
) {
    @JsonProperty("avatar")
    fun getAvatar(): String? {
        return avatarUrl?.let { url ->
            if(avatarUrl.startsWith("/"))
                "${InetAddress.getLoopbackAddress().hostName}$url"
            else
                avatarUrl
        }
    }

    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "user_role", joinColumns = [JoinColumn(name = "user_id")], inverseJoinColumns = [JoinColumn(name = "role_id")])
    var roles: MutableList<Role> = mutableListOf() // Все роли, принадлежащие пользователю

    @JsonIgnore
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, orphanRemoval = true)
    var places: MutableList<Place> = mutableListOf()

    @JsonIgnore
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, orphanRemoval = true)
    var subscriptions: MutableList<Subscription> = mutableListOf()


    constructor(username: String, password: String, email: String, name: String) :
            this(username = username, password = password, email = email, name = name, avatarUrl = null)
}
