package men.brakh.brakhq.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import men.brakh.brakhq.model.enums.FieldType;

import javax.persistence.*;

@Entity
@Table(name="fields")
public class Field {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="queue_id")
    private Queue queue;

    @Column(name="queue_type")
    @Enumerated(EnumType.STRING)
    private FieldType type; // Тип поля

    @Column(name="default_value")
    private String defaultValue;

    @Column(name = "required", columnDefinition = "boolean default true", nullable = false)
    private Boolean required = false;

    public Field() {

    }

    public Field(Queue queue, FieldType type, String defaultValue, Boolean required) {
        this.queue = queue;
        this.type = type;
        this.defaultValue = defaultValue;
        this.required = required;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Queue getQueue() {
        return queue;
    }

    public void setQueue(Queue queue) {
        this.queue = queue;
    }

    public FieldType getType() {
        return type;
    }

    public void setType(FieldType type) {
        this.type = type;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }
}
