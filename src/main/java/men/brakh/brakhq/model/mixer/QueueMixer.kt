package men.brakh.brakhq.model.mixer

import men.brakh.brakhq.model.entity.Queue

interface QueueMixer {
    fun mix(queue: Queue): Queue?
}