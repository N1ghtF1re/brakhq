package men.brakh.brakhq.model.mixer

import men.brakh.brakhq.model.entity.Queue
import men.brakh.brakhq.model.enums.QueueTypes
import org.springframework.stereotype.Component

@Component
class ShuffleQueueMixer : QueueMixer {

    override fun mix(queue: Queue): Queue? {
        if (queue.type == QueueTypes.Random) {
            if(queue.isMixed) return null

            val places = queue.busyPlaces
            val newPlacesNumbers = (1..places.size).toList().shuffled()

            val newPlacesList = places.mapIndexed{ i, place ->
                place.copy(place = newPlacesNumbers[i])
            }

            queue.busyPlaces = newPlacesList
            return queue

        }
        return null
    }
}