package men.brakh.brakhq.model.observer;

interface Observer {
    fun update(event: Enum<*>)
    fun update(event: Enum<*>, obj: Any?)
}
