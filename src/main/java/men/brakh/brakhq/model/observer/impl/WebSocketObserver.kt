package men.brakh.brakhq.model.observer.impl

import com.fasterxml.jackson.databind.ObjectMapper
import men.brakh.brakhq.model.dto.WebSocketEventDto
import men.brakh.brakhq.model.entity.Place
import men.brakh.brakhq.model.entity.Queue
import men.brakh.brakhq.model.enums.QueueEvent
import men.brakh.brakhq.model.observer.Observer
import org.slf4j.LoggerFactory
import org.springframework.web.socket.TextMessage
import org.springframework.web.socket.WebSocketSession
import java.io.IOException

class WebSocketObserver(private val session: WebSocketSession) : Observer {
    private val logger = LoggerFactory.getLogger(WebSocketObserver::class.java)

    override fun update(event: Enum<*>) {
        update(event, null)
    }

    override fun update(event: Enum<*>, obj: Any?) {
        try {
            val queueEvent = event as QueueEvent
            val response: WebSocketEventDto

            response = when(obj) {
                is Place -> WebSocketEventDto(queueEvent, obj)
                is Queue -> WebSocketEventDto(queueEvent, obj)
                else -> WebSocketEventDto(queueEvent)
            }

            val mapper = ObjectMapper()
            val json = mapper.writeValueAsString(response)

            session.sendMessage(TextMessage(json))
        } catch (e: IOException) {
            logger.error("Updating error: ", e)
        }


    }
}
