package men.brakh.brakhq.model.observer

interface Observable {
    fun observe(observer: Observer)
    fun unObserve(observer: Observer)
    fun notify(event: Enum<*>)
    fun notify(event: Enum<*>, obj: Any)
}
