package men.brakh.brakhq.model.serializer

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import men.brakh.brakhq.AppConfig
import men.brakh.brakhq.model.entity.Queue
import men.brakh.brakhq.model.enums.QueueTypes
import java.text.SimpleDateFormat


class OldQueueSerializer(t: Class<Queue>?) : StdSerializer<Queue>(t) {
    constructor() : this(null)

    override fun serialize(queue: Queue, jgen: JsonGenerator, provider: SerializerProvider) {
        val format = SimpleDateFormat(AppConfig.defaultDateFromat)

        jgen.writeStartObject()
        jgen.writeNumberField("id", queue.id)
        jgen.writeStringField("url", queue.url)
        jgen.writeStringField("name", queue.name)
        jgen.writeStringField("description", queue.description)
        jgen.writeStringField("queue_type", queue.type.toString())
        jgen.writeObjectField("owner", queue.owner)
        jgen.writeStringField("reg_start", format.format(queue.regStartDate))
        jgen.writeStringField("reg_end", format.format(queue.regEndDate))
        jgen.writeStringField("event_date", format.format(queue.eventDate))
        jgen.writeNumberField("places_count", queue.placesCount)
        jgen.writeObjectField("busy_places", queue.busyPlaces)
        jgen.writeBooleanField("regActive", queue.isRegActive)
        jgen.writeBooleanField("regEnded", queue.isRegEnded)
        jgen.writeBooleanField("regStarted", queue.isRegStarted)
        jgen.writeBooleanField("mixed", queue.isMixed)
        jgen.writeBooleanField("full", queue.isFull)
        jgen.writeEndObject()


    }

}
class OldQueueDeserializer(t: Class<Queue>?) : StdDeserializer<Queue>(t) {

    constructor() : this(null)

    override fun deserialize(jp: JsonParser, p1: DeserializationContext?): Queue {
        val oc = jp.codec
        val node = oc.readTree<JsonNode>(jp)

        val format = SimpleDateFormat(AppConfig.defaultDateFromat)

        val name: String? = node.get("name")?.asText()
        val description: String? = node.get("description")?.asText()
        val places: Int? = node.get("places_count")?.asInt()
        val regStart = node.get("reg_start")?.asText()?.let { format.parse(it)  }
        val regEnd = node.get("reg_end")?.asText()?.let { format.parse(it)  }
        val eventDate = node.get("event_date")?.asText()?.let { format.parse(it)  }
        val queueType = node.get("queue_type").asText()?.let { QueueTypes.valueOf(it) }

        val queue = Queue()
        queue.name = name
        queue.description = description
        queue.placesCount = places ?: -1
        queue.regStartDate = regStart?.time?.let { java.sql.Timestamp(it) }
        queue.regEndDate = regEnd?.time?.let { java.sql.Timestamp(it) }
        queue.eventDate = eventDate?.time?.let { java.sql.Timestamp(it) }
        queue.type = queueType

        return queue
    }


}