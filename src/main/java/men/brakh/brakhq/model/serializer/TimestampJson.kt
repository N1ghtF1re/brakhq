package men.brakh.brakhq.model.serializer

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import java.sql.Timestamp
import java.util.*

class TimestampSerializer(t: Class<Date>?) : StdSerializer<Date>(t) {
    constructor() : this(null)

    override fun serialize(ts: Date, jg: JsonGenerator, p2: SerializerProvider) {
        jg.writeNumber(ts.time / 1000)
    }
}


class TimestampDeserializer(t: Class<Timestamp>?) : StdDeserializer<Timestamp>(t) {
    constructor() : this(null)

    override fun deserialize(jp: JsonParser, p1: DeserializationContext): Timestamp {
        val unixTimestamp = jp.valueAsLong
        return Timestamp(unixTimestamp * 1000)
    }

}
