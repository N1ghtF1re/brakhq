package men.brakh.brakhq.model.serializer

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import men.brakh.brakhq.AppConfig
import men.brakh.brakhq.model.dto.TokenValidationDto
import java.text.SimpleDateFormat
import java.util.*

class OldTokenValidationJson(t: Class<TokenValidationDto>?) : StdSerializer<TokenValidationDto>(t) {
    constructor() : this(null)

    override fun serialize(resp: TokenValidationDto, jgen: JsonGenerator, provider: SerializerProvider) {
        val format = SimpleDateFormat(AppConfig.defaultDateFromat)

        jgen.writeStartObject()

        jgen.writeStringField("type", resp.type.toString())
        jgen.writeStringField("expires", format.format(resp.expiresDate?.time?.let { Date(it) }))
        jgen.writeBooleanField("expired", resp.isExpired)
        jgen.writeBooleanField("valid", resp.isValid)

        jgen.writeEndObject()


    }

}