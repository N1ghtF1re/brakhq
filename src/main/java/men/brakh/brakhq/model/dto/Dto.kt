package men.brakh.brakhq.model.dto

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import men.brakh.brakhq.model.entity.Place
import men.brakh.brakhq.model.entity.Queue
import men.brakh.brakhq.model.enums.QueueEvent
import men.brakh.brakhq.model.serializer.TimestampSerializer
import men.brakh.brakhq.security.auth.token.JwtToken
import men.brakh.brakhq.security.auth.token.TokenType
import java.util.*

data class UsersQueuesDto(val used: List<Queue>, val created: List<Queue>, val subscribed: List<Queue>)

data class UserDto(val id: Long?, val name: String?, val avatar: String?,
                   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
                   val email: String? = null,
                   @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
                   val username: String? = null
)

data class TokensDto(val token: String, val refreshToken: String, val success: Boolean) {
    constructor(token: JwtToken, refreshToken: JwtToken) : this(token.token, refreshToken.token, true)
}


data class TokenValidationDto(
        val type: TokenType,

        @JsonSerialize(using = TimestampSerializer::class)
        @JsonProperty("expires")
        val expiresDate: Date?,

        @JsonProperty("expired")
        val isExpired: Boolean,

        @JsonProperty("valid")
        val isValid: Boolean
)

class WebSocketEventDto(
        val event: QueueEvent?,

        @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
        val place: Place?,

        @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
        val queue: Queue?
) {
    constructor(event: QueueEvent, place: Place) : this(event, place, null)
    constructor(event: QueueEvent, queue: Queue) : this(event, null, queue)
    constructor(event: QueueEvent) : this(event, null, null)
}
