package men.brakh.brakhq.model.notifications.impl


import com.turo.pushy.apns.ApnsClient
import com.turo.pushy.apns.ApnsClientBuilder
import com.turo.pushy.apns.auth.ApnsSigningKey
import com.turo.pushy.apns.util.ApnsPayloadBuilder
import com.turo.pushy.apns.util.SimpleApnsPushNotification
import com.turo.pushy.apns.util.TokenUtil
import men.brakh.brakhq.config.properties.NotificationsConfig
import men.brakh.brakhq.model.entity.NotificationToken
import men.brakh.brakhq.model.notifications.Notification
import org.slf4j.LoggerFactory
import java.io.File
import java.io.IOException
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import java.util.concurrent.ExecutionException

class AppleNotification(token: NotificationToken, config: NotificationsConfig) : Notification(token, config) {
    private val logger = LoggerFactory.getLogger(AppleNotification::class.java)



    private fun getApnsClient(): ApnsClient {
        return ApnsClientBuilder()
                .setApnsServer(ApnsClientBuilder.DEVELOPMENT_APNS_HOST)
                .setSigningKey(ApnsSigningKey.loadFromPkcs8File(File(config.appleP8Path),
                        config.appleTeamId, config.appleKeyId))
                .build()
    }

    override fun send(title: String, body: String) {
        logger.info("Sent message (APPLE) " + title + ": " + body + " to user with token " + token.token)

        val apnsClient = try {
            getApnsClient()
        } catch (ex: Exception) {
            when(ex) {
                is IOException, is InvalidKeyException, is NoSuchAlgorithmException -> {
                    logger.error("Error with appclient creating", ex)
                    return
                }
                else -> throw ex
            }
        }

        val payloadBuilder = ApnsPayloadBuilder()
        payloadBuilder.setAlertBody(body)

        val payload = payloadBuilder.buildWithDefaultMaximumLength()
        val appleToken = TokenUtil.sanitizeTokenString(token.token)

        val pushNotification = SimpleApnsPushNotification(appleToken, config.appleApplicationName, payload)

        val sendNotificationFuture
                = apnsClient.sendNotification(pushNotification)


        try {
            val pushNotificationResponse = sendNotificationFuture.get()

            if (pushNotificationResponse.isAccepted) {
                logger.info("Push notification accepted by APNs gateway.")
            } else {
                logger.warn("Notification rejected by the APNs gateway: " + pushNotificationResponse.rejectionReason)

                if (pushNotificationResponse.tokenInvalidationTimestamp != null) {
                    logger.warn("\t…and the token is invalid as of " + pushNotificationResponse.tokenInvalidationTimestamp)
                }
            }
        } catch (e: ExecutionException) {
            logger.error("Failed to send push notification.", e)
        } catch (e: InterruptedException) {
            logger.error("Failed to send push notification.", e)
        }

    }


}
