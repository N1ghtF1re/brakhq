package men.brakh.brakhq.model.notifications;

import men.brakh.brakhq.config.properties.NotificationsConfig;
import men.brakh.brakhq.model.entity.NotificationToken;

abstract class Notification(protected var token: NotificationToken, protected var config: NotificationsConfig) {
    abstract fun send(title: String, body: String)
}
