package men.brakh.brakhq.model.notifications

import men.brakh.brakhq.config.properties.NotificationsConfig
import men.brakh.brakhq.model.entity.NotificationToken
import men.brakh.brakhq.model.enums.NotificationTokenType.APPLE_TOKEN
import men.brakh.brakhq.model.notifications.impl.AppleNotification
import org.springframework.stereotype.Component

@Component
open class NotificationFactory (private val config: NotificationsConfig) {
    fun create(token: NotificationToken): Notification? {
        when (token.type) {
            APPLE_TOKEN -> if (config.isAppleEnabled) return AppleNotification(token, config)
        }

        return null
    }
}
