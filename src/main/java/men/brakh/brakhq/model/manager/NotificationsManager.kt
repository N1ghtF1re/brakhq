package men.brakh.brakhq.model.manager

import men.brakh.brakhq.config.properties.NotificationsConfig
import men.brakh.brakhq.extensions.subscribers
import men.brakh.brakhq.model.entity.Queue
import men.brakh.brakhq.model.entity.User
import men.brakh.brakhq.model.enums.QueueEvent
import men.brakh.brakhq.model.notifications.NotificationFactory
import men.brakh.brakhq.service.db.NotificationTokenService
import org.springframework.stereotype.Component

@Component
class NotificationsManager(private val notificationFactory: NotificationFactory,
                           private val notificationsConfig: NotificationsConfig,
                           private val notificationTokenService: NotificationTokenService) {

    private fun notifyUsers(users: List<User>, queue: Queue, message: String) {
        users.asSequence()
                .flatMap { user ->  notificationTokenService.findByUser(user).asSequence()}
                .mapNotNull { notificationToken -> notificationFactory.create(notificationToken) }
                .forEach { notification ->
                    notification.send(queue.name, message)
                }
    }

    fun notifyAllSubscribers(queue: Queue, event: QueueEvent)
            = notifyAllSubscribers( queue, notificationsConfig.queueEventMessages[event]!!)

    fun notifyAllSubscribers(queue: Queue, message: String)
            = notifyUsers( queue.subscribers, queue, message )

}
