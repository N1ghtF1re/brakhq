package men.brakh.brakhq.model.manager;

import men.brakh.brakhq.model.entity.Queue
import men.brakh.brakhq.model.observer.Observer

object QueueEventManager {
    private val observersTable = mutableMapOf<Queue, MutableSet<Observer>>()

    @Synchronized
    fun add(queue: Queue, observer: Observer) {
        if (queue !in observersTable) {
            observersTable[queue] = mutableSetOf()
        }

        val observers = observersTable[queue]!!
        observers.add(observer)
    }

    @Synchronized
    fun remove(queue: Queue, observer: Observer) {
        val observers = observersTable[queue]
        observers?.remove(observer)
    }

    @Synchronized
    fun getObservers(queue: Queue): Set<Observer> {
        return observersTable[queue] ?: setOf()
    }
}
