package men.brakh.brakhq.model.enums;

public enum QueueEvent {
    REG_START,
    REG_END,
    PLACE_TAKE,
    PLACE_FREE,
    QUEUE_CHANGE,
    QUEUE_MIX
}
