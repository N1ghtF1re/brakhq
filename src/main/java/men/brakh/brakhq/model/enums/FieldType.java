package men.brakh.brakhq.model.enums;

public enum FieldType {
    TextArea,
    TextField
}
