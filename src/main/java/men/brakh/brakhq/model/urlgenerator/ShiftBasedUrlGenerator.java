package men.brakh.brakhq.model.urlgenerator;

/**
 * Внимание! Код достат из секретных архивов и как он работает - сложно сказать
 */
public class ShiftBasedUrlGenerator implements UrlGenerator {

    private final String base = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private final int baseLength = base.length();

    // Перевод в 62-ричную  СС
    private String toBase(long num, int b) {
        StringBuilder res = new StringBuilder();

        int r = (int) (num % b);
        res.append(base.charAt(r));
        long q = num/b;
        while (q != 0) {
            r = (int) (q % b);
            q = q/b;
            res.insert(0, base.charAt(r));
        }
        return new String(res);
    }

    private String toBase(long num) {
        return toBase(num, baseLength);
    }

    // Складываем в 62-ричной СС с циклическим сдивгом при переполнении
    private int lol(int b, int chr) {
        chr = b + chr;
        do
            if (chr >= baseLength) {
                chr -= baseLength;
            } else {
                break;
            }
        while(true);
        return chr;

    }

    // Шифруем. А как - секрет. Да и сам я уже не сильно помню
    private String shifr(String str) {
        StringBuilder res = new StringBuilder(str);

        for (int i = str.length(); i < 5; i++) {
            res.insert(0, '0');
        }
        char b = res.charAt(res.length()-1);
        int kek = base.indexOf(b);
        for (int i = 0; i < res.length(); i++){
            res.setCharAt(i, base.charAt(lol(kek, base.indexOf(res.charAt(i)))));
            kek++;
            if (kek >= 62) kek = 0;
        }

        return new String(res);
    }

    @Override
    public String generate(long id) {
        String hash = toBase(id);
        return shifr(hash);
    }
}
