package men.brakh.brakhq.model.urlgenerator;

public interface UrlGenerator {
    String generate(long id);
}
