package men.brakh.brakhq.controllers

import men.brakh.brakhq.exceptions.*
import men.brakh.brakhq.localisation.localeString
import men.brakh.brakhq.security.auth.extractor.TokenExtractor
import men.brakh.brakhq.security.auth.token.RawAccessJwtToken
import men.brakh.brakhq.security.config.JwtSettings
import men.brakh.brakhq.service.feedback.DeveloperConnectionService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.ServletWebRequest
import org.springframework.web.multipart.MaxUploadSizeExceededException
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler


@ControllerAdvice
class ExceptionsController(private val devContact: DeveloperConnectionService,
                           private val tokenExtractor: TokenExtractor,
                           private val jwtSettings: JwtSettings) : ResponseEntityExceptionHandler() {

    private val log: Logger = LoggerFactory.getLogger(ExceptionsController::class.java)
    private val ServletWebRequest.uri: String
        get() = this.request.requestURI

    private val ServletWebRequest.paramsString: String
        get() = request.parameterMap
            .map { (key: String, values: Array<String>) -> "$key: ${values.joinToString(", ")}" }
            .joinToString("; ")


    private fun <T> handleException(body: T, request: ServletWebRequest, statusCode: HttpStatus = HttpStatus.BAD_REQUEST): ResponseEntity<T> {
        log.warn("Handled exception in ${request.uri} with params ${request.paramsString}. Reason: ${body.toString()}.")

        val headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON_UTF8

        return ResponseEntity(body, headers, statusCode)
    }

    @ExceptionHandler(IllegalArgumentException::class)
    fun handleIllegalArgument(ex: IllegalArgumentException, request: ServletWebRequest): ResponseEntity<Map<String, String>>
            = handleException(mapOf("msg" to (ex.message ?: "Illegal argument")), request)

    @ExceptionHandler(NoUserException::class)
    fun handleNoUserException(ex: NoUserException, request: ServletWebRequest): ResponseEntity<Map<String, String>>
            = handleException(mapOf("msg" to localeString("userNotExist")), request, HttpStatus.NOT_FOUND)

    @ExceptionHandler(NoQueueException::class)
    fun handleNoQueueException(ex: NoQueueException, request: ServletWebRequest): ResponseEntity<Map<String, String>>
            = handleException(mapOf("msg" to localeString("queueNotExist")), request, HttpStatus.NOT_FOUND)

    @ExceptionHandler(QueueIsFullException::class)
    fun queueIsFull(ex: QueueIsFullException, req: ServletWebRequest): ResponseEntity<Map<String, String>>
            = handleException(mapOf("msg" to localeString("queueIsFull")), req)

    @ExceptionHandler(InvalidUserChangingException::class)
    fun handleInvalidUserChangingException(ex: InvalidUserChangingException, req: ServletWebRequest):
            ResponseEntity<Map<String, String>> = handleException(ex.errors, req)

    @ExceptionHandler(InvalidUserRegistrationException::class)
    fun handleInvalidUserRegistration(ex: InvalidUserRegistrationException, req: ServletWebRequest):
            ResponseEntity<Map<String, String>> = handleException(ex.errors, req)

    @ExceptionHandler(QueueCreationException::class, QueueUpdatingException::class)
    fun handleQueueCreationException(ex: RuntimeException, req: ServletWebRequest):
            ResponseEntity<Map<String, String>> = handleException(mapOf("msg" to ex.message!!), req)

    @ExceptionHandler(InvalidJwtTokenException::class)
    fun handleInvalidJwtTokenException(ex: InvalidJwtTokenException, req: ServletWebRequest):
            ResponseEntity<Map<String, String>> = handleException(mapOf("msg" to (ex.message ?: "Invalid JWT token")), req)


    @ExceptionHandler(MaxUploadSizeExceededException::class)
    fun handleMaxUploadSizeException(ex: MaxUploadSizeExceededException, req: ServletWebRequest):
            ResponseEntity<Map<String, String>> = handleException(mapOf("msg" to ex.message!!), req)

    @ExceptionHandler(StorageFileNotFoundException::class)
    fun handleFileNotFoundException(ex: StorageFileNotFoundException, req: ServletWebRequest):
            ResponseEntity<Map<String, String>> = handleException(mapOf("msg" to "Not found"), req, HttpStatus.NOT_FOUND)

    @ExceptionHandler(AttemptToEditAlienQueueException::class)
    fun handleAttempToEditAlienQueueException(ex: AttemptToEditAlienQueueException, req: ServletWebRequest):
            ResponseEntity<Map<String, String>> = handleException(mapOf("msg" to
                localeString("userCantChangeQueueOwner")), req, HttpStatus.FORBIDDEN)




    @ExceptionHandler(Exception::class)
    fun allExceptionsHandler(ex: Exception, req: ServletWebRequest): ResponseEntity<Map<String, String>> {
        logger.error("Unexpected error at server", ex)

        val token: String? = req.getHeader("Authorization")

        val user = if(token != null) {
            val tokenPayload = tokenExtractor.extract(token)
            val rawToken = RawAccessJwtToken(tokenPayload)
            val claims = rawToken.parseClaims(jwtSettings.tokenSigningKey)
            claims.body.subject
        } else {
            "none"
        }

        val msg = """
            Request info:
            - URL: [${req.request.method}] ${req.uri}
            - Parameters: ${req.paramsString}.
            - IP: ${req.getHeader("X-FORWARDED-FOR")}
            - User: $user
        """.trimIndent()

        devContact.handleError(ex, msg = msg)

        return handleException(mapOf("msg" to (ex.message ?: ex.toString())), req)
    }
}
