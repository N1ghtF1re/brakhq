package men.brakh.brakhq.controllers.v1;

import men.brakh.brakhq.exceptions.NoUserException;
import men.brakh.brakhq.localisation.TranslatorKt;
import men.brakh.brakhq.model.entity.NotificationToken;
import men.brakh.brakhq.model.entity.User;
import men.brakh.brakhq.model.enums.NotificationTokenType;
import men.brakh.brakhq.model.notifications.Notification;
import men.brakh.brakhq.model.notifications.NotificationFactory;
import men.brakh.brakhq.model.response.ResponseState;
import men.brakh.brakhq.service.db.NotificationTokenService;
import men.brakh.brakhq.service.db.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Random;

@Deprecated
@RestController
@RequestMapping(path="/notifications", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class OldNotificationsController {

    private final UserService userService;


    private final NotificationTokenService notificationTokenService;

    private final NotificationFactory notificationFactory;

    private Logger logger = LoggerFactory.getLogger(OldNotificationsController.class);

    @Autowired
    public OldNotificationsController(UserService userService, NotificationTokenService notificationTokenService, NotificationFactory notificationFactory) {
        this.userService = userService;
        this.notificationTokenService = notificationTokenService;
        this.notificationFactory = notificationFactory;
    }



    @PostMapping()
    public @ResponseBody
    ResponseState subscribe(@RequestParam() NotificationTokenType type,
                            @RequestParam() String token) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        try {
            User currentUser = userService.getUserByAuth(auth);
            logger.info(currentUser.getUsername() + " subscribed to notifications with token " + token);

            NotificationToken notificationToken = new NotificationToken(currentUser, type, token, -1);

            notificationTokenService.save(notificationToken);

            return new ResponseState(true, TranslatorKt.localeString("success"));

        } catch (NoUserException e) {
            return new ResponseState(false, TranslatorKt.localeString("userNotExist")); // User not exist
        }
    }

    @DeleteMapping()
    public @ResponseBody
    ResponseState unsubscribe(@RequestParam() NotificationTokenType type) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        try {
            User currentUser = userService.getUserByAuth(auth);

            notificationTokenService.findByUser(currentUser)
                    .stream()
                    .filter(notificationToken -> notificationToken.getType() == type)
                    .findFirst()
                    .ifPresent(notificationTokenService::delete);


            return new ResponseState(true, TranslatorKt.localeString("success"));

        } catch (NoUserException e) {
            return new ResponseState(false, TranslatorKt.localeString("userNotExist")); // User not exist
        }
    }

    @GetMapping("/test")
    public @ResponseBody
    ResponseState test() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        try {
            User currentUser = userService.getUserByAuth(auth);
            notificationTokenService.findByUser(currentUser).forEach(token -> {
                            Notification notification = notificationFactory.create(token);
                            if(notification != null) {
                                notification.send("TEST" + new Random().nextInt(), "TEST" + new Random().nextInt());
                            }
                        }
            );


            return new ResponseState(true, TranslatorKt.localeString("success"));
        } catch (NoUserException e) {
            return new ResponseState(false, TranslatorKt.localeString("userNotExist")); // User not exist
        } catch (Exception e) {
            logger.error("Notification sending error", e);
            return new ResponseState(false, "FAIL");
        }
    }
}
