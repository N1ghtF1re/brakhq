package men.brakh.brakhq.controllers.v1;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ObjectNode;
import men.brakh.brakhq.exceptions.NoUserException;
import men.brakh.brakhq.extensions.UserExtentsonsKt;
import men.brakh.brakhq.localisation.TranslatorKt;
import men.brakh.brakhq.model.dto.UserDto;
import men.brakh.brakhq.model.entity.Queue;
import men.brakh.brakhq.model.entity.User;
import men.brakh.brakhq.model.response.ModelResponse;
import men.brakh.brakhq.model.serializer.OldQueueDeserializer;
import men.brakh.brakhq.model.serializer.OldQueueSerializer;
import men.brakh.brakhq.service.db.QueueService;
import men.brakh.brakhq.service.db.UserService;
import men.brakh.brakhq.validator.UserValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(path="/api", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Deprecated()
public class OldUserApiController {

    private final QueueService queueService;

    private final UserService userService;

    private final UserValidator userValidator;

    private final ObjectMapper mapper;


    Logger logger = LoggerFactory.getLogger(OldUserApiController.class);

    public OldUserApiController(QueueService queueService, UserService userService, UserValidator userValidator, ObjectMapper mapper) {
        this.queueService = queueService;
        this.userService = userService;
        this.userValidator = userValidator;
        this.mapper = mapper;
    }


    private static ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(Queue.class, new OldQueueSerializer());
        module.addDeserializer(Queue.class, new OldQueueDeserializer());
        objectMapper.registerModule(module);
        return objectMapper;
    }


    /**
     * Return user json
     * @param id User id
     * @return json:
     * {
     *     "success": true/false,
     *     "response": USER_JSON,
     *     "message": "ERROR_MESSAGE" // ONLY IF success = false
     * }
     */
    @GetMapping("user")

    public @ResponseBody
    ModelResponse<UserDto> getUser(@RequestParam(required = false) Long id, @RequestParam(required = false) String username) {
        if(id == null && username == null) {
            return new ModelResponse<>("One of parameters is required");
        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if(auth != null) {
            UserDto user = UserExtentsonsKt.toExtendedDto(userService.findByUsername(auth.getName()));
            if(user.getId().equals(id) || user.getUsername().equalsIgnoreCase(username)) {
                return new ModelResponse<>(user);
            }
        }

        try {
            if(id != null)
                return new ModelResponse<>(UserExtentsonsKt.toDto(userService.findById(id)));
            else {
                return new ModelResponse<>(UserExtentsonsKt.toDto(userService.findByUsername(username)));
            }
        } catch (NoUserException e) {
            return new ModelResponse<>(TranslatorKt.localeString("userNotExist")); // User not exist);
        }
    }

    /**
     * Change user in DB (only user who sent request)
     * @param userEditsJson JSON in request body. Format:
     * {
     *   "name": "NAME",
     *   "avatar": "AVATAR_URL",
     *   "password": "PASSWORD",
     *   "email": "EMAIL"
     * }
     * @see User
     * Allowable User fields for change:
     *      name (can't be empty)
     *      avatar
     *      password
     *      email
     * @return JSON in format:
     * {
     *   "success": true/false,
     *   "errors": STRING ARRAY (ONLY IF SUCCESS = FALSE)
     * }
     */

    @PutMapping("user")
    public @ResponseBody
    ObjectNode changeUser(@RequestBody Map<String, String> userEditsJson) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        ObjectNode objectNode = mapper.createObjectNode();
        try {
            User currentUser = userService.getUserByAuth(auth);

            ObjectNode errorsNode = userValidator.checkErrors(userValidator.validateChanging(userEditsJson));

            if (!errorsNode.equals(objectNode)) {
                return errorsNode;
            }

            currentUser = UserExtentsonsKt.editUser(currentUser, userEditsJson);
            try {
                userService.update(currentUser);
            } catch (Exception e) {
                objectNode.put("success", false);
                objectNode.put("errors", e.getMessage());
                return objectNode;
            }

            objectNode.put("success", true);
            logger.info(currentUser.getUsername() + " changed");

            return objectNode;
        } catch (NoUserException e) {
            objectNode.put("success", false);
            objectNode.put("errors", TranslatorKt.localeString("userNotExist")); // User not exist);
            return objectNode;
        }
    }


    /**
     * Return queues created by user with id
     * @param id User id
     * @return queues list
     */
    @GetMapping(path="/users/{userId}/queues/created")
    public @ResponseBody
    ModelResponse<JsonNode> getCreatedQueues(@PathVariable("userId") long id) {
        try {
            return new ModelResponse<>(objectMapper().valueToTree(queueService.findByOwner(id)));
        } catch (NoUserException e) {
            return new ModelResponse<>(TranslatorKt.localeString("userNotExist"));
        }
    }

    /**
     * Return queues for which the user is registered
     * @param id User id
     * @return queues list
     */
    @GetMapping(path="/users/{userId}/queues/used")
    public @ResponseBody
    ModelResponse<JsonNode> getUsedQueues(@PathVariable("userId") long id) {
        User user;
        try {
            user = userService.findById(id);
        } catch (NoUserException e){
            return new ModelResponse<>(TranslatorKt.localeString("userNotExist"));
        }

        return new ModelResponse<>(objectMapper().valueToTree(UserExtentsonsKt.getUsedQueues(user)));
    }

    /**
     * User registration
     * @param userJson JSON in request body. Format:
     * {
     * 	 "username": "USERNAME",
     * 	 "password": "PASSWORD",
     * 	 "email": "EMAIL"
     * }
     * @return JSON in format:
     * {
     *   "success": true/false,
     *   "errors": STRING ARRAY (ONLY IF SUCCESS = FALSE)
     * }
     */
    @PostMapping(value = "/users/registration")
    public ObjectNode registration(@RequestBody Map<String, String> userJson) {

        ObjectNode objectNode = mapper.createObjectNode();

        ObjectNode errorsNode = userValidator.checkErrors(userValidator.validate(userJson));

        if (!errorsNode.equals(objectNode)) {
            return errorsNode;
        }

        String username = userJson.get("username");
        String password = userJson.get("password");
        String email = userJson.get("email");
        String name = userJson.get("name");

        User user = new User(username, password, email, name);

        userService.save(user);

        objectNode.put("success", true);
        logger.info(user.getUsername() + " registered");

        return objectNode;
    }
}
