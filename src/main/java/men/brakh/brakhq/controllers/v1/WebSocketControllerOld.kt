package men.brakh.brakhq.controllers.v1

import com.fasterxml.jackson.databind.ObjectMapper
import men.brakh.brakhq.model.observer.Observer
import men.brakh.brakhq.model.observer.impl.OldWebsocketObserver
import men.brakh.brakhq.service.db.QueueService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.web.socket.BinaryMessage
import org.springframework.web.socket.CloseStatus
import org.springframework.web.socket.TextMessage
import org.springframework.web.socket.WebSocketSession
import org.springframework.web.socket.handler.AbstractWebSocketHandler
import java.util.*

@Component
@Deprecated("Derecated controller")
class WebSocketControllerOld(private val queueService: QueueService) : AbstractWebSocketHandler() {
    private val logger = LoggerFactory.getLogger(WebSocketControllerOld::class.java)

    private val observers = HashMap<WebSocketSession, Observer>()
    private val queues = HashMap<WebSocketSession, Long>()

    override fun handleTransportError(session: WebSocketSession, throwable: Throwable) {
        val msg = mapOf("error" to throwable.message)
        session.sendMessage(TextMessage(ObjectMapper().writeValueAsString(msg)))
        logger.error("", throwable)
    }

    @Throws(Exception::class)
    override fun afterConnectionClosed(session: WebSocketSession?, status: CloseStatus?) {
        val observer = observers[session] ?: return
        val id = queues[session]
        val dbQueue = id?.let { queueService.findById(it) }

        dbQueue?.unObserve(observer)
        observers.remove(session)
        queues.remove(session)
    }

    override fun afterConnectionEstablished(session: WebSocketSession?) {
        logger.info("New connection to websockets " + session!!.id)
    }

    @Throws(Exception::class)
    private fun handleMessage(message: String, session: WebSocketSession) {
        val mapper = ObjectMapper()
        val json = mapper.readTree(message)

        val dbQueue = when {
            json["id"] != null -> queueService.findById(json["id"].asLong())
            json["url"] != null -> queueService.findByUrl(json["url"].asText())
            else -> throw RuntimeException()
        }

        logger.info(String.format("New observe to queue %s [%d]", dbQueue.name, dbQueue.id))
        val observer = OldWebsocketObserver(session)
        observers[session] = observer
        queues[session] = dbQueue.id
        dbQueue.observe(observer)

        session.sendMessage(TextMessage("Ok"))
    }

    @Throws(Exception::class)
    override fun handleTextMessage(session: WebSocketSession, jsonTextMessage: TextMessage) {
        handleMessage(jsonTextMessage.payload, session)
    }

    @Throws(Exception::class)
    override fun handleBinaryMessage(session: WebSocketSession, message: BinaryMessage) {
        val jsonFromBinary = String(message.payload.array())
        logger.info("Kiryl (or someone else) send $jsonFromBinary as binary")
        handleMessage(jsonFromBinary, session)
    }
}
