package men.brakh.brakhq.controllers.v1;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import men.brakh.brakhq.exceptions.InvalidJwtTokenException;
import men.brakh.brakhq.exceptions.JwtExpiredTokenException;
import men.brakh.brakhq.exceptions.NoUserException;
import men.brakh.brakhq.model.dto.TokenValidationDto;
import men.brakh.brakhq.model.entity.User;
import men.brakh.brakhq.model.serializer.OldTokenValidationJson;
import men.brakh.brakhq.security.auth.UserContext;
import men.brakh.brakhq.security.auth.extractor.TokenExtractor;
import men.brakh.brakhq.security.auth.token.*;
import men.brakh.brakhq.security.config.JwtSettings;
import men.brakh.brakhq.security.oauth.Callbacks;
import men.brakh.brakhq.security.oauth.VkOAuth;
import men.brakh.brakhq.service.db.UserService;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller for authorization and authentication
 */
@Deprecated
@RestController
@RequestMapping(path="/api/auth", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class OldAuthContoller {
    private final TokenExtractor tokenExtractor;

    private final JwtSettings jwtSettings;

    private final JwtTokenFactory tokenFactory;

    private final UserService userService;

    private final VkOAuth vkOAuth;

    private final Callbacks callbacks;

    public OldAuthContoller(TokenExtractor tokenExtractor, JwtSettings jwtSettings, JwtTokenFactory tokenFactory, UserService userService, VkOAuth vkOAuth, Callbacks callbacks) {
        this.tokenExtractor = tokenExtractor;
        this.jwtSettings = jwtSettings;
        this.tokenFactory = tokenFactory;
        this.userService = userService;
        this.vkOAuth = vkOAuth;
        this.callbacks = callbacks;
    }

    private static ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(TokenValidationDto.class, new OldTokenValidationJson());
        objectMapper.registerModule(module);
        return objectMapper;
    }


    /**
     * Token update using refresh token
     * @param rawRefreshToken refreshToken string
     *
     * @throws NoUserException
     * @throws InvalidJwtTokenException
     * @throws InsufficientAuthenticationException
     *
     * @return Updated token json ( { "token" : "..." }
     */
    @GetMapping(value="/token")
    public @ResponseBody
    JwtToken updateToken(@RequestParam(name="refreshToken") String rawRefreshToken,
                         @RequestParam(name="tokenType") TokenType tokenType) throws NoUserException {

        // Достаем refresh token из хэдера
        String tokenPayload = tokenExtractor.extract(rawRefreshToken);


        RawAccessJwtToken rawToken = new RawAccessJwtToken(tokenPayload);
        RefreshToken refreshToken = RefreshToken.create(rawToken, jwtSettings.getRefreshTokenSigningKey()).
                orElseThrow(InvalidJwtTokenException::new);

        String jti = refreshToken.getJti();

        String subject;
        try {
            subject = refreshToken.getSubject();
        } catch (BadCredentialsException e) {
            throw new InvalidJwtTokenException();
        }
        User user = userService.findByUsername(subject);

        if (user.getRoles() == null) throw new InsufficientAuthenticationException("User has no roles assigned");
        List<GrantedAuthority> authorities = user.getRoles().stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getAuthority()))
                .collect(Collectors.toList());

        UserContext userContext = UserContext.create(user.getUsername(), authorities);

        if(tokenType == TokenType.AUTHENTICATION_TOKEN)
            return tokenFactory.createAccessJwtToken(userContext);
        else if(tokenType == TokenType.REFRESH_TOKEN)
            return tokenFactory.createRefreshToken(userContext);
        return null;
    }


    /**
     * Checks token for validity
     * @param token Token string (Refresh token or Authentication token)
     * @return json:
     * {
     *     "valid": true/false,
     *     "expired": true/false,
     *     "expires": STRING IN FORMAT {@link men.brakh.brakhq.AppConfig#defaultDateFromat}
     *     "type": {@link TokenType} value
     * }
     *
     * if valid = false => the token is invalid
     * if valid = true and expired = true => the token was valid but expired
     * if valid = true and expired = false => the token is valid and not expired
     */
    @GetMapping(value="/checkToken")
    public @ResponseBody
    JsonNode checkToken(@RequestParam(name="token") String token) {
        RawAccessJwtToken jwtToken = null;
        try {
            jwtToken = new RawAccessJwtToken(tokenExtractor.extract(token));
        } catch (AuthenticationServiceException e) {
            return objectMapper().valueToTree(new TokenValidationDto(TokenType.UNDEFINED_TOKEN, null, false, false));
        }

        TokenType tokenType;
        String key;
        try {
            if (jwtToken.isSigned(jwtSettings.getTokenSigningKey())) {
                // Токен аутентификации
                key = jwtSettings.getTokenSigningKey();
                tokenType = TokenType.AUTHENTICATION_TOKEN;

            } else if (jwtToken.isSigned(jwtSettings.getRefreshTokenSigningKey())) {
                // Токен обновления
                key = jwtSettings.getRefreshTokenSigningKey();
                tokenType = TokenType.REFRESH_TOKEN;
            } else {
                // Не валидный токен
                return objectMapper().valueToTree(new TokenValidationDto(TokenType.UNDEFINED_TOKEN, null, false, false));
            }
        } catch (Exception e) {
            return objectMapper().valueToTree(new TokenValidationDto(TokenType.UNDEFINED_TOKEN, null, false, false));
        }

        // Если не зашло в else - токен хоть как валидный. Но он либо истекший, либо действительный

        Jws<Claims> claims = null;
        try {
            claims = jwtToken.parseClaims(key);
        } catch (JwtExpiredTokenException e) {
            // Токен то валидный, но истек
            return objectMapper().valueToTree(new TokenValidationDto(tokenType, e.getExpirationTime(), true, true));
        }

        // Если исключения не произошло, то токен валидный и действительный
        Date expiration = claims.getBody().getExpiration();


        return objectMapper().valueToTree(new TokenValidationDto(tokenType, expiration, false, true));
    }


    /**
     * Authorizes the user through VK OAuth and returns the token and token access through the parameters,
     * redirecting to the page {@link men.brakh.brakhq.security.config.VkOAuthConfig#clientCallbackUrl}
     */
    @GetMapping("vk")
    public @ResponseBody
    RedirectView vkAuth(@RequestParam("callback") String callback) throws URISyntaxException {
        RedirectView redirectView = new RedirectView();
        redirectView.setUrl(vkOAuth.getAuthUri(callback).toString());

        return redirectView;
    }

    @GetMapping("vk/callback")
    public @ResponseBody
    RedirectView vkCallBack(@RequestParam String code, @RequestParam("state") String callback) {
        callbacks.add(code, callback);
        RedirectView redirectView = new RedirectView();
        URI uri = vkOAuth.handleToken(code);
        redirectView.setUrl(uri.toString());

        return redirectView;
    }

}
