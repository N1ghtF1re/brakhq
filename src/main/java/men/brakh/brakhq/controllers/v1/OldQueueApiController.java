package men.brakh.brakhq.controllers.v1;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import men.brakh.brakhq.exceptions.NoQueueException;
import men.brakh.brakhq.exceptions.NoUserException;
import men.brakh.brakhq.extensions.QueueExtensionKt;
import men.brakh.brakhq.localisation.TranslatorKt;
import men.brakh.brakhq.model.entity.Place;
import men.brakh.brakhq.model.entity.Queue;
import men.brakh.brakhq.model.entity.User;
import men.brakh.brakhq.model.enums.QueueTypes;
import men.brakh.brakhq.model.enums.QueueEvent;
import men.brakh.brakhq.model.response.ModelResponse;
import men.brakh.brakhq.model.response.ResponseState;
import men.brakh.brakhq.model.serializer.OldQueueDeserializer;
import men.brakh.brakhq.model.serializer.OldQueueSerializer;
import men.brakh.brakhq.service.db.PlaceService;
import men.brakh.brakhq.service.db.QueueService;
import men.brakh.brakhq.service.db.UserService;
import men.brakh.brakhq.validator.QueueValidator;
import org.hibernate.exception.JDBCConnectionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * Controller <b>Queue API</b>.
 * Methods:
 * @see OldQueueApiController#createQueue(Queue)
 * @see OldQueueApiController#getQueueByUrl(String)
 * @see OldQueueApiController#changeQueue(Queue)
 * @see OldQueueApiController#takePlace(Long, Integer)
 * @see OldQueueApiController#freePlace(long)
 */
@Deprecated
@RestController
@RequestMapping(path="/api", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class OldQueueApiController {
    private final QueueService queueService;

    private final PlaceService placeService;

    private final UserService userService;

    private final QueueValidator queueValidator;


    private Logger logger = LoggerFactory.getLogger(OldQueueApiController.class);

    public OldQueueApiController(QueueService queueService, PlaceService placeService, UserService userService, QueueValidator queueValidator) {
        this.queueService = queueService;
        this.placeService = placeService;
        this.userService = userService;
        this.queueValidator = queueValidator;
    }

    private static ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addSerializer(Queue.class, new OldQueueSerializer());
        module.addDeserializer(Queue.class, new OldQueueDeserializer());
        objectMapper.registerModule(module);
        return objectMapper;
    }

    /**
     * Creating new queue
     * @param newQueue - queue object
     * The "id" field (queue id) must be in newQueue object
     * @see Queue
     * Required fields in object of new queue:
     *      owner (required only id, must be equal to auth user)
     *      name (title of new queue, can't be empty)
     *      queue_type
     *      reg_start
     *      event_date
     *      reg_end
     *      places_count
     * @return ModelResponse object
     * @see ModelResponse<Queue>
     */

    @PostMapping("/queue")
    public @ResponseBody
    ModelResponse<JsonNode> createQueue(
			@RequestBody String newQueueString) throws IOException {


        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        try {
            User currentUser = userService.getUserByAuth(auth);

            Queue newQueue = objectMapper().readValue(newQueueString, Queue.class);

            newQueue.setOwner(currentUser);

            String response = queueValidator.validateCreating(newQueue);

            if (response != null) {
                return new ModelResponse<>(response);
            }

            try {
                Queue savedQueue = queueService.save(newQueue);

                return new ModelResponse<>(objectMapper().valueToTree(savedQueue));
            } catch (Exception e) {
                logger.error("Error when saving new queue ", e);
                return new ModelResponse<>(e.getMessage());
            }
        } catch (NoUserException e) {
            return new ModelResponse<>(TranslatorKt.localeString("userNotExist")); // User not exist
        }
    }

    /**
     * Return queue json by URL
     * @param url Queue url
     * @return json
     * {
     *     "success": true/false,
     *     "response": QUEUE_JSON,
     *     "message": "ERROR_MESSAGE" // ONLY IF success = false
     * }
     */
    @GetMapping("/queue")
    public @ResponseBody
    ModelResponse<JsonNode> getQueueByUrl(@RequestParam String url) {
        try {
            return new ModelResponse<>(objectMapper().valueToTree(queueService.findByUrl(url)));
        } catch (NoQueueException e) {
            return new ModelResponse<>(TranslatorKt.localeString("userNotExist"));
        }
    }

    /**
     * Changing queue (only for queue owner)
     * @param changedQueue - queue object
     * @see Queue
     * Required field in object of changed queue:
     *      id
     * Allowable fields for change:
     *      name (title of queue, can't be empty)
     *      event_date
     *      description
     * @return ModelResponse object
     * @see ModelResponse<Queue>
     */

    @PutMapping("/queue")
    public @ResponseBody
    ModelResponse<JsonNode> changeQueue(
            @RequestBody String changedQueueString) throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        try {
            User currentUser = userService.getUserByAuth(auth);

            Queue changedQueue = objectMapper().readValue(changedQueueString, Queue.class);

            Long queueId = changedQueue.getId();

            if (queueId == null) {
                return new ModelResponse<>(TranslatorKt.localeString("paramQueueIdRequired"));
            }

            Queue queue = queueService.findById(queueId);
            Long ownerId = queue.getOwner().getId();

            if (!ownerId.equals(currentUser.getId())) {
                return new ModelResponse<>(TranslatorKt.localeString("userNotQueueOwner"));
            }

            String response = queueValidator.validateChanging(changedQueue);
            if (response != null) {
                return new ModelResponse<>(response);
            }

            QueueExtensionKt.editQueue(queue, changedQueue);

            try {
                return new ModelResponse<>(objectMapper().valueToTree(changedQueue));
            } catch (Exception e) {
                logger.error("Error when updating queue ", e);
                return new ModelResponse<>(e.getMessage()); // Error in updating Queue
            }

        } catch (NoUserException e) {
            return new ModelResponse<>(TranslatorKt.localeString("userNotExist")); // User not exist

        } catch (NoQueueException e) {
            return new ModelResponse<>(TranslatorKt.localeString("queueNotExist")); // Queue not exist
        }
    }


    @DeleteMapping("/queue")
    public @ResponseBody ResponseState deleteQueue(@RequestParam(name="queueId") int queueId) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        try {
            User currentUser = userService.getUserByAuth(auth);
            Queue queue = queueService.findById(queueId);

            Long ownerId = queue.getOwner().getId();

            if (!ownerId.equals(currentUser.getId())) {
                return new ResponseState(false, TranslatorKt.localeString("userNotQueueOwner"));
            }

            if(!queue.isEmpty()) {
                return new ResponseState(false, TranslatorKt.localeString("deletedQueueNotEmpty"));
            }

            queueService.delete(queue);
            return new ResponseState(true, TranslatorKt.localeString("success"));
        } catch (NoUserException e) {
            return new ResponseState(false, TranslatorKt.localeString("userNotExist"));
        } catch (NoQueueException e) {
            return new ResponseState(false, TranslatorKt.localeString("queueNotExist"));
        }
    }

    /**
     * User takes place in queue
     * @param queueId - id of the required queue
     * @param placeNumber - number of take place (Only if queue is not random)
     * @return ResponseState object
     * @see ResponseState
     */

    @PostMapping(path="/queues/{queueId}/places")
    public @ResponseBody ResponseState takePlace(@PathVariable(name = "queueId") Long queueId,
                                                 @RequestParam(name = "place", required = false) Integer placeNumber) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        try {
            User currentUser = userService.getUserByAuth(auth);

            long userId = currentUser.getId();

            try {
                Queue queue = queueService.findById(queueId);


                // Проверки параметров
                if (queue.getType() == QueueTypes.Default && placeNumber == null) {
                    // Для дефолтных очередей обязательно наличие номера места
                    return new ResponseState(false, TranslatorKt.localeString("paramPlaceRequired"));
                } else if (placeNumber == null) {
                    placeNumber = QueueExtensionKt.findFirstUnBusyPlace(queue); // Изначально по порядку
                } else if (queue.getType() == QueueTypes.Default) {
                    if (placeNumber > queue.getPlacesCount() || placeNumber <= 0) {
                        return new ResponseState(false, TranslatorKt.localeString("invalidPlaceNumber"));
                    }
                }

                if (queue.isRegActive()) {
                    // Если заполненных мест больше или равно, чем всего может вместить очередь => отказываем
                    if (queue.isFull()) {
                        return new ResponseState(false, TranslatorKt.localeString("queueIsFull"));
                    }

                    // Пользователь не может занять более одного места в очереди
                    if (placeService.findByQueueIdAndUserId(queue.getId(), userId) != null) {
                        return new ResponseState(false, TranslatorKt.localeString("userHavePlaceInQueue"));
                    }

                    synchronized (placeService) {
                        Queue lastQueueVersion = queueService.findById(queueId);

                        if (QueueExtensionKt.isPlaceUnBusy(lastQueueVersion, placeNumber)) {
                            Place place = placeService.save(new Place(currentUser, queue, placeNumber, -1));
                            queue.notify(QueueEvent.PLACE_TAKE, place);
                            return new ResponseState(true, TranslatorKt.localeString("success"));
                        } else {
                            return new ResponseState(false, TranslatorKt.localeString("placeIsBusy"));
                        }
                    }
                } else {
                    return new ResponseState(false, TranslatorKt.localeString("queueRegNotActive"));
                }
            } catch (NoQueueException e) {
                return new ResponseState(false, TranslatorKt.localeString("queueNotExist")); // Queue not exist
            } catch (JDBCConnectionException e) {
                logger.error("CRITICAL ERROR! DATABASE BROKE DOWN!", e);
                return new ResponseState(false, TranslatorKt.localeString("databaseError"));
            }
        } catch (NoUserException e) {
            return new ResponseState(false, TranslatorKt.localeString("userNotExist")); // User not exist
        }
    }

    /**
     * User free up place in queue
     * @param queueId - id of the required queue
     * @return ResponseState object
     * @see ResponseState
     */
    @PutMapping(path="/queues/{queueId}/places")
    public @ResponseBody ResponseState freePlace(@PathVariable(name="queueId") long queueId) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        try {
            User currentUser = userService.getUserByAuth(auth);

            long userId = currentUser.getId();

            Place place = placeService.findByQueueIdAndUserId(queueId, userId);
            if(place == null) {
                return new ResponseState(false, TranslatorKt.localeString("placeNotExist"));
            }

            try {
                place.getQueue().notify(QueueEvent.PLACE_FREE, place);
                placeService.delete(place.getId());
                return new ResponseState(true, TranslatorKt.localeString("success"));
            } catch (Exception e) {
                logger.error("Error when freeing place", e);
                return new ResponseState(false, e.getMessage()); // Error in delete Place object
            }
        } catch (NoUserException e) {
            return new ResponseState(false, TranslatorKt.localeString("userNotExist")); // User not exist
        }
    }

    /**
     * User takes first unbusy place in queue
     * @param queueId - id of the required queue
     * @return ResponseState object
     * @see ResponseState
     */

    @PostMapping(path="/queues/{queueId}/places/first")
    public @ResponseBody ResponseState takeFirstPlace(@PathVariable(name="queueId") long queueId) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        try {
            User currentUser = userService.getUserByAuth(auth);

            try {
                Queue queue = queueService.findById(queueId);

                boolean isQueueRegActive = queue.isRegActive();
                if (!isQueueRegActive) {
                    return new ResponseState(false, TranslatorKt.localeString("queueRegNotActive"));
                }

                // Пользователь не может занять более одного места в очереди
                if(placeService.findByQueueIdAndUserId(queue.getId(), currentUser.getId()) != null) {
                    return new ResponseState(false, TranslatorKt.localeString("userHavePlaceInQueue"));
                }


                synchronized (placeService) {
                    Queue finalQueueVersion = queueService.findById(queueId);
                    int placeNumber = QueueExtensionKt.findFirstUnBusyPlace(finalQueueVersion);
                    if (placeNumber != 0) {
                        Place place = placeService.save(new Place(currentUser, queue, placeNumber, -1));
                        queue.notify(QueueEvent.PLACE_TAKE, place);
                        return new ResponseState(true, TranslatorKt.localeString("success"));
                    } else {
                        return new ResponseState(false, TranslatorKt.localeString("queueIsFull"));
                    }
                }

            } catch (NoQueueException e) {
                return new ResponseState(false, TranslatorKt.localeString("queueNotExist"));
            } catch (Exception e) {
                logger.error("Error when taking a place ", e);
                return new ResponseState(false, e.getMessage()); // Error in add Place object
            }
        } catch (NoUserException e) {
            return new ResponseState(false, TranslatorKt.localeString("userNotExist")); // User not exist
        }
    }


}
