package men.brakh.brakhq.controllers.v2

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jws
import men.brakh.brakhq.exceptions.InvalidJwtTokenException
import men.brakh.brakhq.exceptions.JwtExpiredTokenException
import men.brakh.brakhq.model.dto.TokenValidationDto
import men.brakh.brakhq.model.entity.User
import men.brakh.brakhq.security.auth.UserContext
import men.brakh.brakhq.security.auth.extractor.TokenExtractor
import men.brakh.brakhq.security.auth.token.*
import men.brakh.brakhq.security.config.JwtSettings
import men.brakh.brakhq.security.oauth.Callbacks
import men.brakh.brakhq.security.oauth.VkOAuth
import men.brakh.brakhq.service.db.UserService
import org.springframework.http.MediaType
import org.springframework.security.authentication.AuthenticationServiceException
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.InsufficientAuthenticationException
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.view.RedirectView

@RestController
@RequestMapping(path = ["/api/v2/auth"], produces = [MediaType.APPLICATION_JSON_UTF8_VALUE])
open class AuthControllerV2(private val tokenExtractor: TokenExtractor,
                            private val jwtSettings: JwtSettings,
                            private val tokenFactory: JwtTokenFactory,
                            private val userService: UserService,
                            private val vkOAuth: VkOAuth,
                            private val callbacks: Callbacks) {


    @ResponseBody
    @GetMapping(value = ["/token"])
    open fun updateToken(@RequestParam(name = "refreshToken") rawRefreshToken: String,
                    @RequestParam(name = "tokenType") tokenType: TokenType): JwtToken? {

        // Extracting refresh token from header
        val tokenPayload = tokenExtractor.extract(rawRefreshToken)

        val rawToken = RawAccessJwtToken(tokenPayload)
        val refreshToken = RefreshToken.create(rawToken, jwtSettings.refreshTokenSigningKey)
                .orElseThrow{ InvalidJwtTokenException() }


        val username: String = try {
            refreshToken.subject
        } catch (e: BadCredentialsException) {
            throw InvalidJwtTokenException()
        }

        val user: User = userService.findByUsername(username)

        if (user.roles.isEmpty()) throw InsufficientAuthenticationException("User has no roles assigned")
        val authorities: List<GrantedAuthority> = user.roles
                .map { authority -> SimpleGrantedAuthority(authority.authority) }
                .toList()

        val userContext = UserContext.create(user.username, authorities)

        return when(tokenType) {
            TokenType.AUTHENTICATION_TOKEN -> tokenFactory.createAccessJwtToken(userContext)
            TokenType.REFRESH_TOKEN -> tokenFactory.createRefreshToken(userContext)
            else -> null
        }
    }

    @GetMapping(value = ["/checkToken"])
    @ResponseBody
    open fun checkToken(@RequestParam(name = "token") token: String): TokenValidationDto {
        fun invalidToken() = TokenValidationDto(TokenType.UNDEFINED_TOKEN, null, false, false)

        val jwtToken: RawAccessJwtToken = try {
            RawAccessJwtToken(tokenExtractor.extract(token))
        } catch (e: AuthenticationServiceException) {
            return invalidToken()
        }

        val (key: String, tokenType:TokenType) = when {
            jwtToken.isSigned(jwtSettings.tokenSigningKey) -> Pair(jwtSettings.tokenSigningKey, TokenType.AUTHENTICATION_TOKEN)
            jwtToken.isSigned(jwtSettings.refreshTokenSigningKey) -> Pair(jwtSettings.refreshTokenSigningKey, TokenType.REFRESH_TOKEN)
            else -> return invalidToken()
        }

        val claims: Jws<Claims> = try {
            jwtToken.parseClaims(key)
        } catch (e: JwtExpiredTokenException) {
            // Token is valid, but expired
            return TokenValidationDto(tokenType, e.expirationTime, true, true)
        }

        // Token is valid and active
        val expiration = claims.body.expiration

        return TokenValidationDto(tokenType, expiration, false, true)
    }

    @GetMapping("vk")
    @ResponseBody
    open fun vkAuth(@RequestParam("callback") callback: String): RedirectView {
        val redirectView = RedirectView()
        redirectView.url = vkOAuth.getAuthUri(callback).toString()

        return redirectView
    }

    @GetMapping("vk/callback")
    @ResponseBody
    open fun vkCallBack(@RequestParam code: String, @RequestParam("state") callback: String): RedirectView {
        callbacks.add(code, callback)
        val redirectView = RedirectView()
        val uri = vkOAuth.handleToken(code)
        redirectView.url = uri.toString()

        return redirectView
    }
}