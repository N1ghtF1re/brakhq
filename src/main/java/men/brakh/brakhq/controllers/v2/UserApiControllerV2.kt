package men.brakh.brakhq.controllers.v2

import men.brakh.brakhq.exceptions.InvalidUserChangingException
import men.brakh.brakhq.exceptions.InvalidUserRegistrationException
import men.brakh.brakhq.extensions.*
import men.brakh.brakhq.model.dto.UserDto
import men.brakh.brakhq.model.dto.UsersQueuesDto
import men.brakh.brakhq.model.entity.User
import men.brakh.brakhq.service.db.UserService
import men.brakh.brakhq.service.storage.StorageService
import men.brakh.brakhq.validator.UserValidator
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile


@RestController
@RequestMapping(path = ["/api/v2"], produces = [MediaType.APPLICATION_JSON_UTF8_VALUE])
open class UserApiControllerV2(private val storageService: StorageService,
                               private val userService: UserService,
                               private val userValidator: UserValidator) {

    @GetMapping("/user")
    @ResponseBody
    open fun getUser(@RequestParam(required = false) id: Long?, @RequestParam(required = false) username: String?): UserDto {

        if (id == null && username == null) throw IllegalArgumentException("One of parameters is required")

        ifAuthorized { user: User ->
            if(id == user.id || username.equals(user.username, ignoreCase = true)) {
                return user.toExtendedDto()
            }
        }

        return if (id != null)
            userService.findById(id).toDto()
        else {
            val user = userService.findByUsername(username!!)
            user.toDto()
        }
    }

    @PutMapping("user")
    @ResponseBody
    open fun changeUser(@RequestBody userEditsJson: Map<String, String>) {

        authorized { user: User ->
            val errors: Map<String, String> = userValidator.validateChanging(userEditsJson)
            if(errors.isNotEmpty()) throw InvalidUserChangingException(errors)

            val changedUser: User = user.editUser(userEditsJson)
            userService.update(changedUser)
        }
    }

    @PostMapping("user/avatar")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    open fun changeAvatar(@RequestParam("file") file: MultipartFile) {
        require(file.isImage()) { "Uploaded file must be image" }

        authorized { user ->
            val filename = "${user.username}.${file.extension}"
            storageService.store(file, filename)

            val absolutePath = "/files/$filename"
            userService.update(user.copy(avatarUrl = absolutePath))
        }
    }


    @GetMapping(path = ["/users/{userId}/queues"])
    @ResponseBody
    open fun getCreatedQueues(@PathVariable("userId") id: Long): UsersQueuesDto {
        val user = userService.findById(id)
        return UsersQueuesDto(
                created = user.createdQueues,
                used = user.usedQueues,
                subscribed = user.subscribedQueues
        )
    }


    @PostMapping(path = ["/users/registration"])
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    open fun registration(@RequestBody userJson: Map<String, String>) {

        val errors: Map<String, String> = userValidator.validate(userJson)
        if(errors.isNotEmpty()) throw InvalidUserRegistrationException(errors)


        val username: String = userJson["username"]!!
        val password: String = userJson["password"]!!
        val email: String = userJson["email"]!!
        val name: String = userJson["name"]!!

        val user = User(username = username, password = password, email = email, name = name)

        userService.save(user)
    }

}