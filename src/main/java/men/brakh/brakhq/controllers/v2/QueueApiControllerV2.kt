package men.brakh.brakhq.controllers.v2

import men.brakh.brakhq.exceptions.AttemptToEditAlienQueueException
import men.brakh.brakhq.exceptions.QueueCreationException
import men.brakh.brakhq.exceptions.QueueUpdatingException
import men.brakh.brakhq.extensions.authorized
import men.brakh.brakhq.extensions.editQueue
import men.brakh.brakhq.extensions.findFirstUnBusyPlace
import men.brakh.brakhq.extensions.isPlaceUnBusy
import men.brakh.brakhq.localisation.localeString
import men.brakh.brakhq.model.entity.Place
import men.brakh.brakhq.model.entity.Queue
import men.brakh.brakhq.model.entity.Subscription
import men.brakh.brakhq.model.entity.User
import men.brakh.brakhq.model.enums.QueueEvent
import men.brakh.brakhq.model.enums.QueueTypes
import men.brakh.brakhq.service.db.PlaceService
import men.brakh.brakhq.service.db.QueueService
import men.brakh.brakhq.service.observers.SubscriptionService
import men.brakh.brakhq.validator.QueueValidator
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping(path = ["/api/v2"], produces = [MediaType.APPLICATION_JSON_UTF8_VALUE])
open class QueueApiControllerV2(private val queueService: QueueService,
                                private val placeService: PlaceService,
                                private val queueValidator: QueueValidator,
                                private val subscriptionService: SubscriptionService) {

    @PostMapping("/queue")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    open fun createQueue(@RequestBody newQueue: Queue): Queue {

        authorized { user: User ->
            newQueue.owner = user
        }

        queueValidator.validateCreating(newQueue)?.let { throw QueueCreationException(it) }

        val queue = queueService.save(newQueue)
        subscriptionService.save(Subscription(user = queue.owner, queue = queue))

        return queue
    }

    @GetMapping("/queue")
    @ResponseBody
    open fun getQueueByUrl(@RequestParam url: String): Queue = queueService.findByUrl(url)


    @PutMapping("/queue")
    @ResponseBody
    open fun changeQueue(@RequestBody changeableQueue: Queue): Queue {

        authorized { user: User ->
            requireNotNull(changeableQueue.id) { localeString("paramQueueIdRequired") }
            val queue: Queue = queueService.findById(changeableQueue.id)
            require(queue.owner.id == user.id) { throw AttemptToEditAlienQueueException() }

            queueValidator.validateChanging(changeableQueue)?.let { throw QueueUpdatingException(it) }

            queue.editQueue(changeableQueue)

            return queue
        }

        throw RuntimeException()
    }

    @DeleteMapping("/queue")
    @ResponseBody
    open fun deleteQueue(@RequestParam(name = "queueId") queueId: Long) {

        authorized { user ->
            val queue = queueService.findById(queueId)
            require(user.id == queue.owner.id) { throw AttemptToEditAlienQueueException() }
            require( queue.isEmpty ) { localeString("deletedQueueNotEmpty") }

            unSubscribeQueue(queueId)
            queueService.delete(queue)
        }
    }

    @PostMapping(path = ["/queues/{queueId}/places"])
    @ResponseBody
    open fun takePlace(@PathVariable(name = "queueId") queueId: Long,
                  @RequestParam(name = "place", required = false) placeNumber: Int?) {

        authorized { user ->
            val queue = queueService.findById(queueId)

            require(queue.type == QueueTypes.Random || placeNumber != null) { localeString("paramPlaceRequired") }
            require( placeNumber == null || (placeNumber <= queue.placesCount && placeNumber > 0)) {
                localeString("invalidPlaceNumber")
            }
            require( queue.isRegActive ) { localeString("queueRegNotActive") }
            require( !queue.isFull ) { localeString("queueIsFull") }
            // User can't take > 1 places

            synchronized(placeService) {
                require(placeService.findByQueueIdAndUserId(queueId, user.id) == null) { localeString("userHavePlaceInQueue") }

                val finalQueue = queueService.findById(queueId)

                val finalPlaceNumber = placeNumber ?: finalQueue.findFirstUnBusyPlace()
                require(finalQueue.isPlaceUnBusy(finalPlaceNumber)) { localeString("placeIsBusy") }

                val place: Place = placeService.save(Place(user = user, queue = queue, place = finalPlaceNumber))
                queue.notify(QueueEvent.PLACE_TAKE, place)
            }

            subscribeQueue(queueId)
        }
    }

    @DeleteMapping(path = ["/queues/{queueId}/places"])
    @ResponseBody
    open fun freePlace(@PathVariable(name = "queueId") queueId: Long) {

        authorized { user: User ->
            val place: Place = placeService.findByQueueIdAndUserId(queueId, user.id) ?: return

            place.queue.notify(QueueEvent.PLACE_FREE, place)
            placeService.delete(place.id)
        }
    }

    @PostMapping(path = ["/queues/{queueId}/places/first"])
    @ResponseBody
    open fun takeFirstPlace(@PathVariable(name = "queueId") queueId: Long) {

        authorized { user: User ->
            val queue = queueService.findById(queueId)

            require(queue.isRegActive) { localeString("queueRegNotActive") }

            synchronized(placeService) {
                // User can't take > 1 places
                require( placeService.findByQueueIdAndUserId(queueId, user.id) == null ) { localeString("userHavePlaceInQueue") }
                val finalQueue = queueService.findById(queueId)
                val placeNumber = finalQueue.findFirstUnBusyPlace()
                require(placeNumber != 0) { localeString("queueIsFull") }

                val place = try {
                    placeService.save(Place(user = user, queue = queue, place = placeNumber))
                } catch (e: Exception) {
                    return takeFirstPlace(queueId)
                }

                queue.notify(QueueEvent.PLACE_TAKE, place)

            }

            subscribeQueue(queueId)
        }
    }

    @PostMapping(path = ["/queues/{queueId}/subscription"])
    @ResponseBody
    open fun subscribeQueue(@PathVariable(name = "queueId") queueId: Long) {

        authorized { user: User ->
            val queue = queueService.findById(queueId)
            try {
                subscriptionService.save(Subscription(user = user, queue = queue))
            } catch (e: Exception) {
                throw IllegalArgumentException(e)
            }
        }
    }

    @DeleteMapping(path = ["/queues/{queueId}/subscription"])
    @ResponseBody
    open fun unSubscribeQueue(@PathVariable(name = "queueId") queueId: Long) {

        authorized { user: User ->
            val queue: Queue = queueService.findById(queueId)
            subscriptionService.deleteByUserAndQueue(user = user, queue = queue)
        }
    }

    

}