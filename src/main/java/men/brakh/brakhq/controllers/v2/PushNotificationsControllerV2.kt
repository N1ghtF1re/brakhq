package men.brakh.brakhq.controllers.v2

import men.brakh.brakhq.extensions.authorized
import men.brakh.brakhq.model.entity.NotificationToken
import men.brakh.brakhq.model.entity.User
import men.brakh.brakhq.model.enums.NotificationTokenType
import men.brakh.brakhq.model.notifications.NotificationFactory
import men.brakh.brakhq.service.db.NotificationTokenService
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping(path = ["api/v2/push-notifications"], produces = [MediaType.APPLICATION_JSON_UTF8_VALUE])
open class PushNotificationsControllerV2(private val notificationTokenService: NotificationTokenService,
                                         private val notificationFactory: NotificationFactory) {

    private val logger = LoggerFactory.getLogger(PushNotificationsControllerV2::class.java)

    @PutMapping
    @ResponseBody
    open fun subscribe(@RequestParam type: NotificationTokenType,
                  @RequestParam token: String) {

        authorized { user: User ->
            logger.info(user.username + " subscribed to notifications with token " + token)
            val notificationToken = NotificationToken(user = user, type =  type, token = token)
            notificationTokenService.save(notificationToken)
        }
    }

    @DeleteMapping
    @ResponseBody
    open fun unsubscribe(@RequestParam type: NotificationTokenType) {

        authorized { user: User ->
            notificationTokenService.findByUser(user)
                    .asSequence()
                    .filter { notificationToken -> notificationToken.type == type }
                    .firstOrNull()
                    ?.let {  notificationTokenService.delete(it) }
        }
    }

    @GetMapping("/test")
    @ResponseBody
    open fun test() {
        authorized { user: User ->
            notificationTokenService.findByUser(user)
                    .forEach { token ->
                        notificationFactory.create(token)
                                ?.send("TEST" + Random().nextInt(), "TEST" + Random().nextInt())

                    }
        }
    }

}