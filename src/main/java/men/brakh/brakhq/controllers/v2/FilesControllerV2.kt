package men.brakh.brakhq.controllers.v2

import men.brakh.brakhq.service.storage.StorageService
import org.springframework.core.io.Resource
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.nio.file.Files


@RestController
@RequestMapping(path = ["/files"])
open class FilesControllerV2(private val storageService: StorageService) {

    @GetMapping("/{filename:.+}")
    @ResponseBody
    open fun serveFile(@PathVariable filename: String): ResponseEntity<Resource> {
        val file = storageService.loadAsResource(filename)
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.filename + "\"")
                .header(HttpHeaders.CONTENT_TYPE, Files.probeContentType(storageService.load(filename)))
                .body<Resource>(file)
    }

}