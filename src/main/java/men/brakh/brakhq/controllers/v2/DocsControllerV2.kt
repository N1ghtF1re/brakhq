package men.brakh.brakhq.controllers.v2

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.view.RedirectView

@Controller
open class DocsControllerV2 {

    @GetMapping("/api/v2/docs")
    @ResponseBody
    open fun docsRedirect() = RedirectView("https://app.swaggerhub.com/apis-docs/N1ghtF1re/BrakhQ/2.0.0")
}