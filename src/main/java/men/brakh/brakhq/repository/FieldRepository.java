package men.brakh.brakhq.repository;

import men.brakh.brakhq.model.entity.Field;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface FieldRepository  extends JpaRepository<Field, Long> {
    Optional<Field> findById(Long id);
}
