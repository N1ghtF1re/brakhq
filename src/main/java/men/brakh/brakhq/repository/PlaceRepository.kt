package men.brakh.brakhq.repository

import men.brakh.brakhq.model.entity.Place
import org.springframework.data.jpa.repository.JpaRepository

interface PlaceRepository : JpaRepository<Place, Long> {
    fun findByQueueIdAndUserId(queueId: Long, userId: Long): Place?
}
