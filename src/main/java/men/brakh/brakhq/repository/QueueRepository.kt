package men.brakh.brakhq.repository;

import men.brakh.brakhq.model.entity.Queue
import men.brakh.brakhq.model.entity.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

interface QueueRepository : JpaRepository<Queue, Long> {
    fun findByUrl(url: String): Queue?
    fun findByOwner(user: User): List<Queue>  // Все очереди пользователя

    fun findByRegEndDateBetweenOrderByRegStartDateAsc(date1: Date, date2: Date): List<Queue>
    fun findByRegStartDateBetweenOrderByRegStartDateAsc(date1: Date, date2: Date): List<Queue>

    @Query(value = "SELECT q FROM Queue q WHERE q.regEndDate < :date AND q.type = 'Random' AND q.isMixed = false")
    fun findNotMixedBeforeNow(@Param(value = "date") date: Date): List<Queue>
}