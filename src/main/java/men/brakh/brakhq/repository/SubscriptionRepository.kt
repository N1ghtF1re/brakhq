package men.brakh.brakhq.repository

import men.brakh.brakhq.model.entity.Queue
import men.brakh.brakhq.model.entity.Subscription
import men.brakh.brakhq.model.entity.User
import org.springframework.data.jpa.repository.JpaRepository

interface SubscriptionRepository : JpaRepository<Subscription, Long> {
    fun findByUser(user: User): List<Subscription>
    fun findByQueue(queue: Queue): List<Subscription>
    fun deleteByUserAndQueue(user: User, queue: Queue)
}