package men.brakh.brakhq.repository;

import men.brakh.brakhq.model.entity.NotificationToken
import men.brakh.brakhq.model.entity.User
import org.springframework.data.jpa.repository.JpaRepository

interface NotificationTokenRepository : JpaRepository<NotificationToken, Long> {
    fun findByUser(user: User): List<NotificationToken>
    fun findByTokenEquals(token: String): NotificationToken?
}
