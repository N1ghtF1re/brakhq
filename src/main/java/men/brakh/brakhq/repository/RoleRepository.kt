package men.brakh.brakhq.repository;

import men.brakh.brakhq.model.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

interface RoleRepository : JpaRepository<Role, Long>