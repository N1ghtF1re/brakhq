package men.brakh.brakhq.service.queuetimers.timers.impl

import men.brakh.brakhq.extensions.tomorrow
import men.brakh.brakhq.model.entity.Queue
import java.util.*

class QueueEndTimer(action: (queue: Queue) -> Unit) : AbstractBasicQueueTimer(action) {
    override fun Queue.getTimerDate(): Date = this.regEndDate
    override fun isNeedToAdd(queue: Queue): Boolean = !queue.isRegEnded && queue.regEndDate < Date().tomorrow()
}