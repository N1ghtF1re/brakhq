package men.brakh.brakhq.service.queuetimers.timers.impl

import men.brakh.brakhq.extensions.tomorrow
import men.brakh.brakhq.model.entity.Queue
import java.util.*

class QueueBeforeStartTimer(private val minBefore: Int = 10, action: (queue: Queue) -> Unit) : AbstractBasicQueueTimer(action) {
    override fun Queue.getTimerDate(): Date {
        val cal = Calendar.getInstance()
        cal.time = this.regStartDate
        cal.add(Calendar.MINUTE, -minBefore)

        return cal.time
    }
    override fun isNeedToAdd(queue: Queue): Boolean  = !queue.isRegStarted && queue.regStartDate <= Date().tomorrow()
}