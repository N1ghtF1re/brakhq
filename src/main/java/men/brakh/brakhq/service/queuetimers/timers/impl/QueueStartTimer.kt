package men.brakh.brakhq.service.queuetimers.timers.impl

import men.brakh.brakhq.extensions.tomorrow
import men.brakh.brakhq.model.entity.Queue
import java.util.*

class QueueStartTimer(action: (queue: Queue) -> Unit) : AbstractBasicQueueTimer(action) {
    override fun Queue.getTimerDate(): Date = this.regStartDate
    override fun isNeedToAdd(queue: Queue): Boolean  = !queue.isRegStarted && queue.regStartDate <= Date().tomorrow()
}