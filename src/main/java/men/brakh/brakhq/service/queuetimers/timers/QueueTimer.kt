package men.brakh.brakhq.service.queuetimers.timers

import men.brakh.brakhq.model.entity.Queue

interface QueueTimer {
    val action: (queue: Queue) -> Unit
    fun isNeedToAdd(queue: Queue): Boolean
    fun add(queue: Queue)
    fun addAll(queues: List<Queue>)
    fun remove(queue: Queue)
}