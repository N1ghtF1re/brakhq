package men.brakh.brakhq.service.queuetimers.timers.impl

import men.brakh.brakhq.model.entity.Queue
import men.brakh.brakhq.service.queuetimers.timers.QueueTimer
import org.slf4j.LoggerFactory
import java.util.*
import kotlin.concurrent.timerTask

/**
 * Timer Wrapper for canceling (needed in this task)
 */
internal class CancelableTimer: Timer() {
    private var currTimer: Timer = Timer()

    override fun purge() = currTimer.purge()
    override fun schedule(p0: TimerTask?, p1: Long) = currTimer.schedule(p0, p1)
    override fun schedule(p0: TimerTask?, p1: Date?) = currTimer.schedule(p0, p1)
    override fun schedule(p0: TimerTask?, p1: Long, p2: Long) = currTimer.schedule(p0, p1, p2)
    override fun schedule(p0: TimerTask?, p1: Date?, p2: Long) = currTimer.schedule(p0, p1, p2)
    override fun scheduleAtFixedRate(p0: TimerTask?, p1: Long, p2: Long) = currTimer.scheduleAtFixedRate(p0, p1, p2)
    override fun scheduleAtFixedRate(p0: TimerTask?, p1: Date?, p2: Long) = currTimer.scheduleAtFixedRate(p0, p1, p2)

    override fun cancel() {
        currTimer.cancel()
        currTimer = Timer()
    }
}

/**
 * Based on basic java timer with cancelable wrapper
 */
abstract class AbstractBasicQueueTimer(override val action: (queue: Queue) -> Unit) : QueueTimer {
    private val logger = LoggerFactory.getLogger(AbstractBasicQueueTimer::class.java)

    private val queuesQueue: SortedSet<Queue> = sortedSetOf(comparator = compareBy { it.getTimerDate() })
    private val timer: Timer = CancelableTimer()

    protected abstract fun Queue.getTimerDate(): Date


    private fun reschedule() {
        val firstQueue = queuesQueue.firstOrNull() ?: return

        val timerName = this.javaClass.simpleName

        timer.cancel()
        timer.schedule(
                timerTask {
                    logger.info("Action ( Timer $timerName ) with queue ${firstQueue.name} [${firstQueue.id}]")
                    action(firstQueue)
                    queuesQueue.remove(firstQueue)
                    reschedule()
                }, firstQueue.getTimerDate()
        )
    }

    override fun add(queue: Queue) {
        logger.info("Queue ${queue.name} [${queue.id}] added to ${this.javaClass.simpleName}")
        queuesQueue.add(queue)
        reschedule()

    }

    override fun addAll(queues: List<Queue>) {
        logger.info("Queues ${queues.map { "${it.name} [${it.id}]" }} added to ${this.javaClass.simpleName}")
        queuesQueue.addAll(queues)
        reschedule()
    }

    override fun remove(queue: Queue) {
        queuesQueue.remove(queue)
        reschedule()
    }
}


