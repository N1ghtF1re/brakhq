package men.brakh.brakhq.service.queuetimers

import men.brakh.brakhq.AppConfig
import men.brakh.brakhq.extensions.mix
import men.brakh.brakhq.localisation.localeString
import men.brakh.brakhq.model.entity.Queue
import men.brakh.brakhq.model.enums.QueueEvent
import men.brakh.brakhq.model.manager.NotificationsManager
import men.brakh.brakhq.service.db.QueueService
import men.brakh.brakhq.service.queuetimers.timers.QueueTimer
import men.brakh.brakhq.service.queuetimers.timers.impl.QueueBeforeStartTimer
import men.brakh.brakhq.service.queuetimers.timers.impl.QueueEndTimer
import men.brakh.brakhq.service.queuetimers.timers.impl.QueueStartTimer
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.util.*

@Service
class QueueTimersServiceImpl(private val notificationsManager: NotificationsManager,
                             private val queueService: QueueService): QueueTimersService {


    private val logger = LoggerFactory.getLogger(QueueTimersServiceImpl::class.java)

    /**
     * Available timers
     */
    override val timers: List<QueueTimer> = listOf(
        QueueStartTimer{ queue ->
            val dbQueue = queueService.findByIdOrNull(queue.id!!) ?: return@QueueStartTimer
            dbQueue.notify(QueueEvent.REG_START)
            notificationsManager.notifyAllSubscribers(dbQueue, QueueEvent.REG_START)
        }, QueueEndTimer { queue ->
            val dbQueue = queueService.findByIdOrNull(queue.id!!) ?: return@QueueEndTimer
            dbQueue.mix()
            dbQueue.notify(QueueEvent.REG_END)
            notificationsManager.notifyAllSubscribers(dbQueue, QueueEvent.REG_END)
        }, QueueBeforeStartTimer { queue ->
            val dbQueue = queueService.findByIdOrNull(queue.id!!) ?: return@QueueBeforeStartTimer
            notificationsManager.notifyAllSubscribers(dbQueue, localeString("queueStartedSoon"))
        }
    )

    @Scheduled(cron = "0 0 0 ? * *", zone = AppConfig.defaultTimeZone)
    override fun sheduledUpdate() {
        logger.info("Scheduled timers updating")
        queueService.findTodayStartedQueues().let(this::addAll)
        queueService.findTodayEndedQueues().let(this::addAll)
    }

    override fun init() {
        val needMix = queueService.findNotMixedBeforeNow()
        needMix.forEach{ it.mix() }

        logger.info("Queues timers service initialised")

        queueService.findTodayStartedQueues().filter { it.regStartDate >= Date() }. let(this::addAll)
        queueService.findTodayEndedQueues().filter { it.regEndDate >= Date() }.let(this::addAll)
    }

    override fun add(queue: Queue) = timers.forEach {if(it.isNeedToAdd(queue)) it.add(queue)}

    override fun addAll(queues: List<Queue>) {
        timers.forEach { timer ->
            val neededToAddQueues = queues.filter { queue -> timer.isNeedToAdd(queue) }
            if(neededToAddQueues.isNotEmpty())
                timer.addAll(neededToAddQueues)
        }

    }

    override fun update(queue: Queue) = timers.forEach { it.remove(queue); it.add(queue) }
    override fun remove(queue: Queue) = timers.forEach { it.remove(queue) }
}