package men.brakh.brakhq.service.queuetimers

import men.brakh.brakhq.model.entity.Queue
import men.brakh.brakhq.service.queuetimers.timers.QueueTimer

interface QueueTimersService {
    val timers: List<QueueTimer>

    fun init()
    fun add(queue: Queue)
    fun addAll(queues: List<Queue>)
    fun update(queue: Queue)
    fun remove(queue: Queue)

    fun sheduledUpdate()
}