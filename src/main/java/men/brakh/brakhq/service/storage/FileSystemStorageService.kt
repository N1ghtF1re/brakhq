package men.brakh.brakhq.service.storage

import men.brakh.brakhq.config.properties.StorageProperties
import men.brakh.brakhq.exceptions.StorageException
import men.brakh.brakhq.exceptions.StorageFileNotFoundException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.Resource
import org.springframework.core.io.UrlResource
import org.springframework.stereotype.Service
import org.springframework.util.FileSystemUtils
import org.springframework.util.StringUtils
import org.springframework.web.multipart.MultipartFile
import java.io.IOException
import java.net.MalformedURLException
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardCopyOption
import java.util.stream.Stream


@Service
class FileSystemStorageService @Autowired
constructor(properties: StorageProperties) : StorageService {
    private var logger = LoggerFactory.getLogger(FileSystemStorageService::class.java)
    private val rootLocation = Paths.get(properties.location)

    override fun store(file: MultipartFile) = store(file, file.originalFilename!!)

    override fun store(file: MultipartFile, rawFilename: String) {
        val filename = StringUtils.cleanPath(rawFilename)
        try {
            if (file.isEmpty) {
                throw StorageException("Failed to store empty file $filename")
            }
            if (filename.contains("..")) {
                // This is a security check
                throw StorageException(
                        "Cannot store file with relative path outside current directory $filename")
            }
            file.inputStream.use { inputStream ->
                Files.copy(inputStream, this.rootLocation.resolve(filename),
                        StandardCopyOption.REPLACE_EXISTING)
            }
        } catch (e: IOException) {
            logger.error("Store file exception: ", e)
            throw StorageException("Failed to store file $filename: ${e.message}", e)
        }
    }

    override fun loadAll(): Stream<Path> {
        try {
            return Files.walk(this.rootLocation, 1)
                    .filter { path -> path != this.rootLocation }
                    .map { this.rootLocation.relativize(it) }
        } catch (e: IOException) {
            e.printStackTrace()
            throw StorageException("Failed to read stored files", e)
        }

    }

    override fun load(filename: String): Path {
        return rootLocation.resolve(filename)
    }

    override fun loadAsResource(filename: String): Resource {
        try {
            val file = load(filename)
            val resource = UrlResource(file.toUri())
            return if (resource.exists() || resource.isReadable) {
                resource
            } else {
                throw StorageFileNotFoundException("Could not read file: $filename")
            }
        } catch (e: MalformedURLException) {
            throw StorageFileNotFoundException("Could not read file: $filename", e)
        }

    }

    override fun deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile())
    }

    override fun init() {
        try {
            if(!Files.exists(rootLocation)) {
                logger.info("${rootLocation.toAbsolutePath()} created")
                Files.createDirectories(rootLocation)
            }
        } catch (e: IOException) {
            throw StorageException("Could not initialize storage", e)
        }

    }
}
