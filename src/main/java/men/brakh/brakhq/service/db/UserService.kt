package men.brakh.brakhq.service.db;

import men.brakh.brakhq.model.entity.User
import org.springframework.security.core.Authentication

interface UserService {
    fun save(user: User): User
    fun update(user: User)

    fun findByUsername(username: String): User
    fun findByUsernameOrNull(username: String): User?
    fun findByEmail(email: String): User
    fun findByEmailOrNull(email: String): User?
    fun findById(id: Long): User

    fun getUserByAuth(auth: Authentication): User
}
