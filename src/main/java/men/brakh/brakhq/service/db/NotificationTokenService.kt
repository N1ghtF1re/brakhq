package men.brakh.brakhq.service.db

import men.brakh.brakhq.model.entity.NotificationToken
import men.brakh.brakhq.model.entity.User

interface NotificationTokenService {
    fun save(token: NotificationToken): NotificationToken
    fun findByUser(user: User): List<NotificationToken>
    fun delete(token: NotificationToken)
}
