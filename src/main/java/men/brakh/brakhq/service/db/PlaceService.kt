package men.brakh.brakhq.service.db;

import men.brakh.brakhq.model.entity.Place

interface PlaceService {
    fun findById(id: Long): Place
    fun findByIdOrNull(id: Long): Place?
    fun findByQueueIdAndUserId(queueId: Long, userId: Long): Place?
    fun save(place: Place): Place
    fun delete(id: Long)
}
