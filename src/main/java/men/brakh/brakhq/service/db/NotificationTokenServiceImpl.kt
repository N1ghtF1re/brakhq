package men.brakh.brakhq.service.db

import men.brakh.brakhq.model.entity.NotificationToken
import men.brakh.brakhq.model.entity.User
import men.brakh.brakhq.repository.NotificationTokenRepository
import org.springframework.stereotype.Service

@Service
class NotificationTokenServiceImpl(private val notificationTokenRepository: NotificationTokenRepository) : NotificationTokenService {

    override fun save(token: NotificationToken): NotificationToken {
        val notificationToken: NotificationToken? = notificationTokenRepository.findByTokenEquals(token.token)

        return if (notificationToken == null) {
            notificationTokenRepository.save(token)
        } else {
            val updatedToken = NotificationToken(token.user, token.type, token.token, notificationToken.id)
            notificationTokenRepository.save(updatedToken)
        }

    }

    override fun findByUser(user: User): List<NotificationToken> = notificationTokenRepository.findByUser(user)
    override fun delete(token: NotificationToken) = notificationTokenRepository.delete(token)

}
