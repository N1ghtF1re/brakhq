package men.brakh.brakhq.service.db

import men.brakh.brakhq.model.entity.Queue
import men.brakh.brakhq.model.entity.User

interface QueueService {
    fun save(queue: Queue): Queue
    fun delete(queue: Queue)
    fun update(queue: Queue)

    fun findById(id: Long): Queue
    fun findByIdOrNull(id: Long): Queue?
    fun findByUrl(url: String): Queue

    fun findByOwner(userId: Long): List<Queue>
    fun findByOwner(userName: String): List<Queue>
    fun findByOwner(user: User): List<Queue>

    fun findAllQueues(): List<Queue>


    fun findTodayEndedQueues(): List<Queue>
    fun findTodayStartedQueues(): List<Queue>

    fun findNotMixedBeforeNow(): List<Queue>
}
