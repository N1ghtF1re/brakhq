package men.brakh.brakhq.service.db

import men.brakh.brakhq.config.SpringContext
import men.brakh.brakhq.exceptions.NoQueueException
import men.brakh.brakhq.extensions.tomorrow
import men.brakh.brakhq.model.entity.Queue
import men.brakh.brakhq.model.entity.User
import men.brakh.brakhq.model.urlgenerator.UrlGenerator
import men.brakh.brakhq.repository.QueueRepository
import men.brakh.brakhq.service.queuetimers.QueueTimersService
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.*
import javax.validation.constraints.NotNull

@Service
class QueueServiceImpl(private val userService: UserService,
                       private val queueRepository: QueueRepository,
                       private val urlGenerator: UrlGenerator
) : QueueService {

    private val queueTimersService: QueueTimersService by lazy { SpringContext.getBean(QueueTimersService::class.java) }

    override fun save(queue: Queue): Queue {
        val savedQueue = queueRepository.save(queue)
        savedQueue.generateUrl(urlGenerator)

        queueTimersService.add(savedQueue)

        return queueRepository.save(savedQueue)
    }

    override fun update(queue: Queue): Unit = run { queueRepository.save(queue); queueTimersService.update(queue) }
    override fun delete(queue: Queue) {
        queueTimersService.remove(queue)
        queueRepository.delete(queue)
    }

    override fun findById(id: Long): Queue = queueRepository.findByIdOrNull(id) ?: throw NoQueueException()
    override fun findByIdOrNull(id: Long): Queue? = queueRepository.findByIdOrNull(id)
    override fun findByUrl(url: String): Queue =  queueRepository.findByUrl(url) ?: throw NoQueueException()

    override fun findByOwner(userId: Long): List<Queue> {
        val owner: User = userService.findById(userId)
        return queueRepository.findByOwner(owner)
    }

    override fun findByOwner(userName: String): List<Queue> {
        val owner: User = userService.findByUsername(userName)
        return queueRepository.findByOwner(owner)
    }

    override fun findByOwner(@NotNull user: User): List<Queue> = queueRepository.findByOwner(user)

    override fun findAllQueues(): List<Queue> = queueRepository.findAll()


    override fun findTodayEndedQueues(): List<Queue> =
            queueRepository.findByRegEndDateBetweenOrderByRegStartDateAsc(Date().todayDate, Date().tomorrowDate)


    override fun findTodayStartedQueues(): List<Queue> =
            queueRepository.findByRegStartDateBetweenOrderByRegStartDateAsc(Date().todayDate, Date().tomorrowDate)


    override fun findNotMixedBeforeNow(): List<Queue> = queueRepository.findNotMixedBeforeNow(Date())
}

private val Date.tomorrowDate: Date
    get() {
        val cal = Calendar.getInstance()
        cal.time = this.tomorrow()
        cal.set(Calendar.MINUTE, 1)

        return cal.time
    }

private val Date.todayDate: Date
    get() {
        val cal = Calendar.getInstance()
        cal.time = this
        cal.add(Calendar.MINUTE, -1)
        return cal.time
    }