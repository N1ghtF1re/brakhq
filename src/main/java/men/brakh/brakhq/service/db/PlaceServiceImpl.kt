package men.brakh.brakhq.service.db

import men.brakh.brakhq.exceptions.NoPlaceException
import men.brakh.brakhq.model.entity.Place
import men.brakh.brakhq.repository.PlaceRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class PlaceServiceImpl(private val placeRepository: PlaceRepository) : PlaceService {

    override fun findByIdOrNull(id: Long): Place? = placeRepository.findByIdOrNull(id)

    override fun findById(id: Long): Place = placeRepository.findByIdOrNull(id) ?: throw NoPlaceException()

    override fun findByQueueIdAndUserId(queueId: Long, userId: Long): Place? =
            placeRepository.findByQueueIdAndUserId(queueId, userId)

    @Synchronized
    override fun save(place: Place): Place = placeRepository.save(place)

    @Synchronized
    override fun delete(id: Long) = findByIdOrNull(id)?.let { placeRepository.delete(it) } ?: Unit
}
