package men.brakh.brakhq.service.db

import men.brakh.brakhq.exceptions.NoUserException
import men.brakh.brakhq.model.entity.User
import men.brakh.brakhq.repository.RoleRepository
import men.brakh.brakhq.repository.UserRepository
import org.springframework.security.core.Authentication
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

@Service
class UserServiceImpl(private val userRepository: UserRepository,
                      private val roleRepository: RoleRepository,
                      private val bCryptPasswordEncoder: BCryptPasswordEncoder
) : UserService {


    override fun save(user: User): User {
        val finalUser = user.copy(password = bCryptPasswordEncoder.encode(user.password))
        finalUser.roles.add(roleRepository.getOne(1L))

        return userRepository.save(finalUser)
    }

    override fun update(user: User): Unit = run { userRepository.save(user) }
    override fun findByUsername(username: String): User = userRepository.findByUsername(username) ?: throw NoUserException()
    override fun findByUsernameOrNull(username: String): User? = userRepository.findByUsername(username)
    override fun findByEmail(email: String): User = userRepository.findByEmail(email) ?: throw NoUserException()
    override fun findByEmailOrNull(email: String): User? = userRepository.findByEmail(email)
    override fun findById(id: Long): User = userRepository.findById(id).orElseThrow{ NoUserException() }
    override fun getUserByAuth(auth: Authentication): User = findByUsername(auth.name)
}
