package men.brakh.brakhq.service.observers

import men.brakh.brakhq.model.entity.Queue
import men.brakh.brakhq.model.entity.Subscription
import men.brakh.brakhq.model.entity.User
import men.brakh.brakhq.repository.SubscriptionRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
open class SubscriptionServiceImpl(private val subscriptionRepository: SubscriptionRepository) : SubscriptionService {
    override fun findByQueue(queue: Queue): List<Subscription> = subscriptionRepository.findByQueue(queue)
    @Transactional
    override fun deleteByUserAndQueue(user: User, queue: Queue) = subscriptionRepository.deleteByUserAndQueue(user, queue)
    override fun delete(subscription: Subscription) = subscriptionRepository.delete(subscription)
    override fun save(subscription: Subscription): Subscription = subscriptionRepository.save(subscription)
}