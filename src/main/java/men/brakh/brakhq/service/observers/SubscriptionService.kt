package men.brakh.brakhq.service.observers

import men.brakh.brakhq.model.entity.Queue
import men.brakh.brakhq.model.entity.Subscription
import men.brakh.brakhq.model.entity.User

interface SubscriptionService {
    fun save(subscription: Subscription): Subscription
    fun delete(subscription: Subscription)
    fun deleteByUserAndQueue(user: User, queue: Queue)
    fun findByQueue(queue: Queue): List<Subscription>
}