package men.brakh.brakhq.service.feedback


interface DeveloperConnectionService {
    fun handleError(ex: Throwable, msg: String? = null)
    fun write(s: String)
}