package men.brakh.brakhq.service.feedback

import men.brakh.brakhq.config.properties.DeveloperContactConfig
import org.apache.http.client.utils.URIBuilder
import org.springframework.stereotype.Service
import java.net.URL

@Service
class DeveloperConnectionViaVkService(val contactConfig: DeveloperContactConfig) : DeveloperConnectionService {
    private fun generateUrl(msg: String): URL {
        return URIBuilder().setScheme("https").setHost("api.vk.com").setPath("/method/messages.send")
                .setParameter("access_token", contactConfig.token)
                .setParameter("v", "5.41")
                .setParameter("user_ids", contactConfig.chatId.toString())
                .setParameter("message", msg)
                .build()
                .toURL()
    }

    override fun write(s: String) {
        val url = generateUrl(s)

        val answer = url.readText()

        println(answer)
    }

    override fun handleError(ex: Throwable, msg: String?) {
        val text = """
            Incomprehensible error on server.

            Exception: ${ex.javaClass.name}
            Exception Message: ${ex.message}
            Additional Message: $msg

            Stacktrace first function: ${ex.stackTrace.firstOrNull()}
        """

        write(text)
    }
}