package men.brakh.brakhq.config.properties

import men.brakh.brakhq.localisation.localeString
import men.brakh.brakhq.model.enums.QueueEvent
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource
import java.util.*

@Configuration
@PropertySource("classpath:notifications.properties")
open class NotificationsConfig {
    @Value("\${apple.p8path}")
    lateinit var appleP8Path: String

    @Value("\${apple.teamId}")
    lateinit var appleTeamId: String

    @Value("\${apple.keyId}")
    lateinit var appleKeyId: String

    @Value("\${apple.application.name}")
    lateinit var appleApplicationName: String

    @Value("\${apple.enabled}")
    var isAppleEnabled: Boolean = false

    val queueEventMessages: Map<QueueEvent, String>
        get() {
            val map = HashMap<QueueEvent, String>()
            map[QueueEvent.REG_END] = localeString("queueEnded")
            map[QueueEvent.REG_START] = localeString("queueStarted")
            return map
        }
}
