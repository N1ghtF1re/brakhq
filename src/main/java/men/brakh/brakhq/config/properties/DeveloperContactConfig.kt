package men.brakh.brakhq.config.properties;

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:feedback.properties")
open class DeveloperContactConfig {
    @Value("\${vk.chat}")
    var chatId: Long = -1

    @Value("\${vk.token}")
    lateinit var token: String
}
