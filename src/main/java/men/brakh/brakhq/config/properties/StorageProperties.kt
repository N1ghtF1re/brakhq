package men.brakh.brakhq.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration


@Configuration
@ConfigurationProperties(prefix = "storage")
open class StorageProperties {
    lateinit var location: String
}