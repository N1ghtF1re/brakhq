package men.brakh.brakhq.config

import men.brakh.brakhq.controllers.v2.WebSocketControllerV2
import men.brakh.brakhq.controllers.v1.WebSocketControllerOld
import org.springframework.context.annotation.Configuration
import org.springframework.web.socket.config.annotation.EnableWebSocket
import org.springframework.web.socket.config.annotation.WebSocketConfigurer
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry

@Configuration
@EnableWebSocket
open class WebSocketConfig(private val webSocketControllerOld: WebSocketControllerOld,
                           private val webSocketController: WebSocketControllerV2) : WebSocketConfigurer {

    override fun registerWebSocketHandlers(registry: WebSocketHandlerRegistry) {
        registry.addHandler(webSocketControllerOld, "/ws/queues").setAllowedOrigins("*")
        registry.addHandler(webSocketController, "/ws/v2/queues").setAllowedOrigins("*")
    }
}