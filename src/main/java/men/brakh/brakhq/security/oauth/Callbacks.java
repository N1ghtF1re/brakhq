package men.brakh.brakhq.security.oauth;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

@Component
public class Callbacks {
    private Logger logger = LoggerFactory.getLogger(Callbacks.class);
    private Map<String, URI> callbacks = new HashMap<>();

    public void add(String code, String url) {
        try {
            URI uri = new URI(url);
            callbacks.put(code, uri);

        } catch (URISyntaxException e) {
            logger.error(url, e);
        }
    }

    public URI getUri(String code) {
        URI uri = callbacks.get(code);
        callbacks.remove(code);
        return uri;
    }
}
