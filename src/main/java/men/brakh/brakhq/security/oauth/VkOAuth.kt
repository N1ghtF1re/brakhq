package men.brakh.brakhq.security.oauth

import com.fasterxml.jackson.databind.JsonNode
import men.brakh.brakhq.exceptions.OAuthException
import men.brakh.brakhq.model.entity.User
import men.brakh.brakhq.security.config.VkOAuthConfig
import men.brakh.brakhq.security.oauth.handler.CallbackOAuthErrorHandler
import men.brakh.brakhq.security.oauth.handler.CallbackOAuthSuccessHandler
import men.brakh.brakhq.security.oauth.jsonreader.JsonReader
import org.apache.http.client.utils.URIBuilder
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.io.IOException
import java.net.URI
import java.net.URISyntaxException
import java.util.*

@Component
class VkOAuth(private val vkOAuthConfig: VkOAuthConfig,
              private val errorHandler: CallbackOAuthErrorHandler,
              private val successHandler: CallbackOAuthSuccessHandler,
              private val callbacks: Callbacks
) : OAuth {

    private val logger = LoggerFactory.getLogger(VkOAuth::class.java)


    @Throws(URISyntaxException::class)
    override
            /**
             * Getting api for authorisation
             */
    fun getAuthUri(callback: String): URI {
        val builder = URIBuilder()
        builder.setScheme(vkOAuthConfig.scheme)
                .setHost(vkOAuthConfig.autHost)
                .setPath("/authorize")
                .setParameter("client_id", vkOAuthConfig.clientId)
                .setParameter("redirect_uri", vkOAuthConfig.redirectUrl)
                .setParameter("scope", vkOAuthConfig.scope.joinToString(","))
                .setParameter("state", callback)
                .setParameter("v", vkOAuthConfig.apiVersion)

        return builder.build()
    }

    /**
     * Getting uri to receive access token
     * @param code Code received at authorisation
     * @return uri
     * @throws URISyntaxException
     */
    @Throws(URISyntaxException::class)
    private fun getAccessTokenUri(code: String): URI {
        val builder = URIBuilder()
        builder.setScheme(vkOAuthConfig.scheme).setHost(vkOAuthConfig.autHost).setPath("/access_token")
                .setParameter("client_id", vkOAuthConfig.clientId)
                .setParameter("client_secret", vkOAuthConfig.clientSecret)
                .setParameter("redirect_uri", vkOAuthConfig.redirectUrl)
                .setParameter("code", code)
                .setParameter("v", vkOAuthConfig.apiVersion)

        return builder.build()
    }

    /**
     * Getting uri to USER API
     * @param accessToken Access token ([VkOAuth.extractAccessTokenAndEmail])
     * @return uri
     * @throws URISyntaxException
     */
    @Throws(URISyntaxException::class)
    private fun getApiUri(accessToken: String): URI {
        val builder = URIBuilder()
        builder.setScheme(vkOAuthConfig.scheme).setHost(vkOAuthConfig.apiHost).setPath("/method/users.get")
                .setParameter("access_token", accessToken)
                .setParameter("v", vkOAuthConfig.apiVersion)
                .setParameter("fields", "photo_max")
        return builder.build()
    }

    /**
     * Makes a request ([VkOAuth.getAccessTokenUri]) for a token and processes it
     * @param code Code received at authorisation
     * @return Map with email and access token
     * @throws URISyntaxException
     * @throws IOException
     * @throws OAuthException
     */
    @Throws(URISyntaxException::class, IOException::class, OAuthException::class)
    private fun extractAccessTokenAndEmail(code: String): Pair<String, String> {
        val uri = getAccessTokenUri(code)

        val jsonObj = JsonReader(uri).read()

        if (jsonObj.has("error")) {
            throw OAuthException(jsonObj.get("error_description").asText())
        }

        val email = jsonObj?.get("email")?.asText() ?: throw OAuthException("Email is not present in VK response. Response: $jsonObj")
        val token = jsonObj.get("access_token").asText()
        return Pair(token, email)
    }

    /**
     * Getting user fields from api ([VkOAuth.getApiUri] and making User object
     * @param code Code received at authorisation
     * @return User object
     * @throws OAuthException
     * @throws IOException
     * @throws URISyntaxException
     */
    @Throws(OAuthException::class, IOException::class, URISyntaxException::class)
    private fun extractUser(code: String): User {
        val (token: String, email: String) = extractAccessTokenAndEmail(code)

        val userApi: URI = getApiUri(token)

        val jsonObj: JsonNode = JsonReader(userApi).read()

        if (jsonObj.has("error")) {
            val errorNode = jsonObj.get("error")
            throw OAuthException(errorNode.get("error_msg").asText())
        }

        val jsonUser: JsonNode = jsonObj.path("response").get(0)

        val name = "${jsonUser.get("first_name").asText()} ${jsonUser.get("last_name").asText()}"

        val password: String = Integer.toHexString(Random().nextInt(jsonUser.get("id").asInt())) + token

        val avatar: String = jsonUser.get("photo_max").asText()

        return User(name = name, password = password, email = email, username = email, avatarUrl = avatar, from = "vk")
    }

    override
            /**
             * Attempts to get a token and returns the URL to which the client will return after all.
             * In the parameters of this url there will be either an error or a token and a refresh token
             *
             * @param code Code received at authorisation
             */
    fun handleToken(code: String): URI {
        val callback = callbacks.getUri(code)

        try {
            val user = extractUser(code)
            return successHandler.handle(callback, user)
        } catch (e: OAuthException) {
            return errorHandler.handle(callback, e)
        } catch (e: IOException) {
            logger.error("Error with VKOAuth ", e)
            return errorHandler.handle(callback, e)
        } catch (e: URISyntaxException) {
            logger.error("Error with VKOAuth ", e)
            return errorHandler.handle(callback, e)
        }

    }

}
