package men.brakh.brakhq.security.oauth.handler;

import men.brakh.brakhq.security.config.VkOAuthConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Component
public class CallbackOAuthErrorHandler {

    private final VkOAuthConfig vkOAuthConfig;

    public CallbackOAuthErrorHandler(VkOAuthConfig vkOAuthConfig) {
        this.vkOAuthConfig = vkOAuthConfig;
    }

    public URI handle(URI callback, Exception e) {
        return UriComponentsBuilder
                .fromUri(callback)
                .queryParam("error", e.getMessage())
                .build().toUri();
    }
}
