package men.brakh.brakhq.security.oauth.handler

import men.brakh.brakhq.model.entity.Role
import men.brakh.brakhq.model.entity.User
import men.brakh.brakhq.security.auth.UserContext
import men.brakh.brakhq.security.auth.token.JwtToken
import men.brakh.brakhq.security.auth.token.JwtTokenFactory
import men.brakh.brakhq.security.config.VkOAuthConfig
import men.brakh.brakhq.service.db.UserService
import org.slf4j.LoggerFactory
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component
import org.springframework.web.util.UriComponentsBuilder
import java.net.URI


@Component
class CallbackOAuthSuccessHandler(private val userService: UserService, private val vkOAuthConfig: VkOAuthConfig, private val jwtTokenFactory: JwtTokenFactory) {


    private val logger = LoggerFactory.getLogger(CallbackOAuthSuccessHandler::class.java)

    private fun createUri(callback: URI, auth: JwtToken, refresh: JwtToken, user: User): URI {
        return UriComponentsBuilder
                .fromUri(callback)
                .queryParam("token", auth.token)
                .queryParam("refresh_token", refresh.token)
                .queryParam("username", user.username)
                .queryParam("id", user.id)
                .build().toUri()
    }

    fun handle(callback: URI, receivedUser: User): URI {
        val user: User? = userService.findByUsernameOrNull(receivedUser.email)


        val savedUser: User = if (user == null) {
            userService.save(receivedUser)
        } else {
            userService.save(user.copy(avatarUrl = receivedUser.avatarUrl))
        }

        val authorities = savedUser.roles
                .map { authority: Role -> SimpleGrantedAuthority(authority.authority) }


        val userContext = UserContext.create(savedUser.username, authorities)

        val authToken = jwtTokenFactory.createAccessJwtToken(userContext)
        val refreshToken = jwtTokenFactory.createRefreshToken(userContext)

        return createUri(callback, authToken, refreshToken, savedUser)
    }
}
