package men.brakh.brakhq.security.oauth;

import java.net.URI;
import java.net.URISyntaxException;

public interface OAuth {

    URI getAuthUri(String callback) throws URISyntaxException;
    URI handleToken(String code);
}
