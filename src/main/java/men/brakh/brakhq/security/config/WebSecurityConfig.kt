package men.brakh.brakhq.security.config

import men.brakh.brakhq.extensions.getAuthorizedUser
import men.brakh.brakhq.security.auth.TokenAuthenticationService
import men.brakh.brakhq.security.auth.filters.JWTAuthenticationFilter
import men.brakh.brakhq.security.auth.filters.JWTLoginFilter
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.annotation.web.configurers.oauth2.client.OAuth2LoginConfigurer
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.CorsConfigurationSource
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import javax.servlet.http.HttpServletResponse
import javax.servlet.ServletException
import java.io.IOException
import javax.servlet.http.HttpServletRequest
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.stereotype.Component


@Configuration
@EnableWebSecurity
open class WebSecurityConfig(private val tokenAuthenticationService: TokenAuthenticationService,
                             private val userDetailsService: UserDetailsService,
                             private val authenticationFilter: JWTAuthenticationFilter
) : WebSecurityConfigurerAdapter() {

    companion object {
        private val APIS = listOf("/api/", "/api/v2/")

        const val AUTHENTICATION_HEADER_NAME = "Authorization"

        private const val AUTHENTICATION_URL = "auth"
        private const val NOTIFICATIONS_URL = "/notifications**"

        private const val REGISTRATION_URL= "users/registration"

        private val AVAILABLE_ENDPOINTS = listOf(REGISTRATION_URL)
    }

    @Bean
    open fun bCryptPasswordEncoder(): BCryptPasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {



        val permitAllEndpointList = AVAILABLE_ENDPOINTS
                .flatMap { endpoint ->
                    APIS.map { api -> "$api$endpoint" }
                }

        

        http
                .cors()
                .and()
                    .csrf().disable() // We don't need CSRF for JWT based authentication
                    .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    //
                .and()
                //
                // ALLOWED NOT GET ENDPOINTS
                    .authorizeRequests()
                    .antMatchers(*permitAllEndpointList.toTypedArray())
                    .permitAll()
                //
                // ALL GET REQUEST ALLOWED
                    .antMatchers(HttpMethod.GET)
                    .permitAll()
                .and()
                //
                // OTHER REQUESTS AUTHORIZED
                    .authorizeRequests()
                    .antMatchers(*APIS.map { "$it**" }.toTypedArray()).authenticated() // Protected API End-points
                //
                .and()
                    .authorizeRequests()
                    .antMatchers(NOTIFICATIONS_URL).authenticated()
                .and()
                    .addFilterBefore(authenticationFilter, UsernamePasswordAuthenticationFilter::class.java)


        APIS.forEach { api ->
            val authUrl = "$api$AUTHENTICATION_URL"
            http.addFilterBefore(JWTLoginFilter(authUrl, authenticationManager(), tokenAuthenticationService),
                    UsernamePasswordAuthenticationFilter::class.java)

        }

        http.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint())

    }

    @Autowired
    @Throws(Exception::class)
    fun configureGlobal(auth: AuthenticationManagerBuilder) {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder())
    }

    @Bean
    open fun corsConfigurationSource(): CorsConfigurationSource {
        val source = UrlBasedCorsConfigurationSource()
        val corsConfiguration = CorsConfiguration().applyPermitDefaultValues()
        corsConfiguration.addAllowedMethod(CorsConfiguration.ALL)
        source.registerCorsConfiguration("/**", corsConfiguration)
        return source
    }

    @Bean
    open fun authenticationEntryPoint() =
            AuthenticationEntryPoint{ _ , resp, _  ->
                resp.status = if(getAuthorizedUser() == null) {
                    HttpStatus.UNAUTHORIZED.value()
                } else {
                    HttpStatus.FORBIDDEN.value()
                }
            }
}

