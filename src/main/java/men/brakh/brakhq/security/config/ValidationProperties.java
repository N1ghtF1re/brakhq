package men.brakh.brakhq.security.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "validation")
public class ValidationProperties {


    private int usernameMinSize;

    private int usernameMaxSize;

    private int passwordMinSize;

    // STRINGS:

    private String passwordEmptyMsg;

    private String usernameEmptyMsg;

    private String usernameSizeMsg;

    private String usernameDuplicateMsg;

    private String passwordSizeMsg;

    private String emailEmptyMsg;

    private String emailInvalidMsg;

    private String emailExistMsg;

    public String getEmailExistMsg() {
        return emailExistMsg;
    }

    public void setEmailExistMsg(String emailExistMsg) {
        this.emailExistMsg = emailExistMsg;
    }

    public String getEmailInvalidMsg() {
        return emailInvalidMsg;
    }

    public void setEmailInvalidMsg(String emailInvalidMsg) {
        this.emailInvalidMsg = emailInvalidMsg;
    }

    public String getEmailEmptyMsg() {
        return emailEmptyMsg;
    }

    public void setEmailEmptyMsg(String emailEmptyMsg) {
        this.emailEmptyMsg = emailEmptyMsg;
    }

    public String getUsernameDuplicateMsg() {
        return usernameDuplicateMsg;
    }

    public void setUsernameDuplicateMsg(String usernameDuplicateMsg) {
        this.usernameDuplicateMsg = usernameDuplicateMsg;
    }

    public String getPasswordSizeMsg() {
        return String.format(passwordSizeMsg, passwordMinSize);
    }

    public void setPasswordSizeMsg(String passwordSizeMsg) {
        this.passwordSizeMsg = passwordSizeMsg;
    }

    public String getUsernameSizeMsg() {
        return String.format(usernameSizeMsg, usernameMinSize, usernameMaxSize);
    }

    public void setUsernameSizeMsg(String usernameSizeMsg) {
        this.usernameSizeMsg = usernameSizeMsg;
    }

    public int getUsernameMinSize() {
        return usernameMinSize;
    }

    public void setUsernameMinSize(int usernameMinSize) {
        this.usernameMinSize = usernameMinSize;
    }

    public int getPasswordMinSize() {
        return passwordMinSize;
    }

    public void setPasswordMinSize(int passwordMinSize) {
        this.passwordMinSize = passwordMinSize;
    }

    public int getUsernameMaxSize() {
        return usernameMaxSize;
    }

    public void setUsernameMaxSize(int usernameMaxSize) {
        this.usernameMaxSize = usernameMaxSize;
    }

    public String getPasswordEmptyMsg() {
        return passwordEmptyMsg;
    }

    public void setPasswordEmptyMsg(String passwordEmptyMsg) {
        this.passwordEmptyMsg = passwordEmptyMsg;
    }

    public String getUsernameEmptyMsg() {
        return usernameEmptyMsg;
    }

    public void setUsernameEmptyMsg(String usernameEmptyMsg) {
        this.usernameEmptyMsg = usernameEmptyMsg;
    }
}
