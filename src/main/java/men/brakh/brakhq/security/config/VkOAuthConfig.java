package men.brakh.brakhq.security.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "vk.oauth")
public class VkOAuthConfig {
    private String scheme;
    private String autHost;
    private String apiHost;
    private String apiVersion;
    private String redirectUrl;
    private String clientId;
    private String clientSecret;
    private String clientCallbackUrl; // URL, на который вернется токен и рефреш токен после успешной авторизации
    private List<String> scope;

    public String getClientCallbackUrl() {
        return clientCallbackUrl;
    }

    public void setClientCallbackUrl(String clientCallbackUrl) {
        this.clientCallbackUrl = clientCallbackUrl;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getAutHost() {
        return autHost;
    }

    public void setAutHost(String autHost) {
        this.autHost = autHost;
    }

    public String getApiHost() {
        return apiHost;
    }

    public void setApiHost(String apiHost) {
        this.apiHost = apiHost;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public List<String> getScope() {
        return scope;
    }

    public void setScope(List<String> scope) {
        this.scope = scope;
    }
}
