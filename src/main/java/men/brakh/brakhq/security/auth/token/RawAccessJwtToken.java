package men.brakh.brakhq.security.auth.token;

import io.jsonwebtoken.*;
import men.brakh.brakhq.exceptions.JwtExpiredTokenException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.BadCredentialsException;

public class RawAccessJwtToken implements JwtToken {
    private static Logger logger = LoggerFactory.getLogger(RawAccessJwtToken.class);
            
    private String token;
    
    public RawAccessJwtToken(String token) {
        this.token = token;
    }

    /**
     * Parses and validates JWT Token signature.
     * 
     * @throws BadCredentialsException
     *
     * 
     */
    public Jws<Claims> parseClaims(String signingKey, boolean logging) {
        try {
            return Jwts.parser().setSigningKey(signingKey).parseClaimsJws(token);
        } catch (UnsupportedJwtException | MalformedJwtException | IllegalArgumentException | SignatureException ex) {
            if(logging)
                logger.error("Invalid JWT Token" + token);
            throw new BadCredentialsException("Invalid JWT token: ", ex);
        } catch (ExpiredJwtException expiredEx) {
            if(logging)
                logger.info("JWT Token is expired: " + expiredEx.getMessage());
            throw new JwtExpiredTokenException(this, "JWT Token expired", expiredEx);

        }
    }


    public Jws<Claims> parseClaims(String signingKey) {
        return parseClaims(signingKey, true);
    }

    public boolean isSigned(String signingKey) {
        try {
            parseClaims(signingKey, false);
        } catch (BadCredentialsException e) {
            return false;
        } catch(JwtExpiredTokenException e) {
            return true;
        }
        return true;
    }

    @Override
    public String getToken() {
        return token;
    }
}
