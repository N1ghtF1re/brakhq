package men.brakh.brakhq.security.auth.token;

public interface JwtToken {
    String getToken();
}
