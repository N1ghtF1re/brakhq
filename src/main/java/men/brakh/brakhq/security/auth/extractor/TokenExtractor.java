package men.brakh.brakhq.security.auth.extractor;

public interface TokenExtractor {
    public String extract(String payload);
}
