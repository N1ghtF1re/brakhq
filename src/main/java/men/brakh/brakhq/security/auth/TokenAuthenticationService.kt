package men.brakh.brakhq.security.auth

import com.fasterxml.jackson.databind.ObjectMapper
import io.jsonwebtoken.Jwts
import men.brakh.brakhq.model.dto.TokensDto
import men.brakh.brakhq.security.auth.extractor.TokenExtractor
import men.brakh.brakhq.security.auth.token.JwtTokenFactory
import men.brakh.brakhq.security.config.JwtSettings
import men.brakh.brakhq.security.config.WebSecurityConfig
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.web.WebAttributes
import org.springframework.stereotype.Component
import java.io.IOException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class TokenAuthenticationService(private val mapper: ObjectMapper,
                                 private val jwtSettings: JwtSettings,
                                 private val jwtTokenFactory: JwtTokenFactory,
                                 private val jwtHeaderTokenExtractor: TokenExtractor) {

    private val logger = LoggerFactory.getLogger(TokenAuthenticationService::class.java)


    /**
     * Return json with tokens
     */
    fun addAuthentication(request: HttpServletRequest, response: HttpServletResponse, authentication: Authentication) {

        val user = UserContext(authentication.principal as org.springframework.security.core.userdetails.User)

        logger.info(user.username + " authorised")

        // Создаем токены
        val accessToken = jwtTokenFactory.createAccessJwtToken(user)
        val refreshToken = jwtTokenFactory.createRefreshToken(user)

        val tokensResponse = TokensDto(accessToken, refreshToken)

        response.status = HttpStatus.OK.value()
        response.contentType = MediaType.APPLICATION_JSON_VALUE
        try {
            mapper.writeValue(response.writer, tokensResponse)
        } catch (e: IOException) {
            response.status = HttpStatus.BAD_REQUEST.value()
        }

        clearAuthenticationAttributes(request)

    }

    /**
     * Check token in head and authentication
     */
    fun getAuthentication(request: HttpServletRequest): Authentication? {
        val token = request.getHeader(WebSecurityConfig.AUTHENTICATION_HEADER_NAME)
        if (token != null) {
            // parse the token.
            val user = Jwts.parser()
                    .setSigningKey(jwtSettings.tokenSigningKey)
                    .parseClaimsJws(jwtHeaderTokenExtractor.extract(token))
                    .body
                    .subject

            return if (user != null) UsernamePasswordAuthenticationToken(user, null,
                    emptyList<GrantedAuthority>()) else null
        }
        return null
    }

    private fun clearAuthenticationAttributes(request: HttpServletRequest) {
        val session = request.getSession(false) ?: return

        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION)
    }

}