package men.brakh.brakhq.security.auth.filters;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import men.brakh.brakhq.security.auth.TokenAuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Component
public class JWTAuthenticationFilter extends GenericFilterBean {
    Logger logger = LoggerFactory.getLogger(JWTAuthenticationFilter.class);

    private final TokenAuthenticationService tokenAuthenticationService;

    public JWTAuthenticationFilter(TokenAuthenticationService tokenAuthenticationService) {
        this.tokenAuthenticationService = tokenAuthenticationService;
    }

    @Override
    /**
     * RETURN 403 IF TOKEN INVALID
     */
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        logger.debug((request.getRequestURI() + " " + request.getMethod()));

        Authentication authentication = null;

        try {
            authentication = tokenAuthenticationService
                    .getAuthentication((HttpServletRequest) servletRequest);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        } catch (ExpiredJwtException | MalformedJwtException | SignatureException
                | UnsupportedJwtException | IllegalArgumentException e) {
            logger.info("Attempt to unauthorized access to " + ((HttpServletRequest) servletRequest).getRequestURI());


            /*// В случае ошибки при обработке токена
            HttpServletResponse response = ((HttpServletResponse) servletResponse);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return;*/
        }



        filterChain.doFilter(servletRequest, servletResponse);
    }

}