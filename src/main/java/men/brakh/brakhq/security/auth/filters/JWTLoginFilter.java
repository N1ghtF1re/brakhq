package men.brakh.brakhq.security.auth.filters;


import com.fasterxml.jackson.databind.ObjectMapper;
import men.brakh.brakhq.model.response.ResponseState;
import men.brakh.brakhq.security.auth.TokenAuthenticationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;


public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {

    private TokenAuthenticationService tokenAuthenticationService;


    public JWTLoginFilter(String url, AuthenticationManager authManager, TokenAuthenticationService tokenAuthenticationService) {
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authManager);
        this.tokenAuthenticationService = tokenAuthenticationService;
    }

    @Override
    /**
     * Authentication by username and password in get request
     *
     * in case valid authentication returns json:
     * {
     *     "success": true,
     *     "token": "TOKEN",
     *     "refreshToken": "TOKEN"
     * }
     *
     * in case error returns:
     * {
     *     "success": false,
     *     "message": "MESSAGE"
     * }
     */
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {


        String username = request.getParameter("username");
        String password = request.getParameter("password");

        logger.info("Attempt Authentication: " + username);

        try {
            return getAuthenticationManager()
                    .authenticate(new UsernamePasswordAuthenticationToken(username, password, Collections.emptyList()));
        } catch (InternalAuthenticationServiceException | BadCredentialsException e) {

            // Если произошло исключение => возвращаем JSON
            ObjectMapper mapper = new ObjectMapper();
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);

            try {
                mapper.writeValue(response.getWriter(), new ResponseState(false, "Wrong username/password"));
            } catch (IOException e1) {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
            }

            return null;
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {

        // Return tokens in json
        tokenAuthenticationService.addAuthentication(request, response, authResult);
    }

}