package men.brakh.brakhq.security.auth.extractor;

import men.brakh.brakhq.security.config.JwtSettings;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.stereotype.Component;

/**
 * Class for extraction token from head
 */
@Component
public class JwtHeaderTokenExtractor implements TokenExtractor {
    private final JwtSettings jwtSettings;

    public JwtHeaderTokenExtractor(JwtSettings jwtSettings) {
        this.jwtSettings = jwtSettings;
    }

    @Override
    public String extract(String header) {
        if (StringUtils.isBlank(header)) {
            throw new AuthenticationServiceException("Authorization header cannot be blank!");
        }

        if (header.length() < jwtSettings.getTokenPrefix().length()) {
            throw new AuthenticationServiceException("Invalid authorization header size.");
        }
        // Удаляем префикс
        return header.replace(jwtSettings.getTokenPrefix(), "");
    }
}
