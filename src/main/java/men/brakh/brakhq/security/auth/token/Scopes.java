package men.brakh.brakhq.security.auth.token;

/**
 * Scopes
 *
 */
public enum Scopes {
    REFRESH_TOKEN;
    
    public String authority() {
        return "ROLE_" + this.name();
    }
}
