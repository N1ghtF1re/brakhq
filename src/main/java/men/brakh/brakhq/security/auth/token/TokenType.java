package men.brakh.brakhq.security.auth.token;

public enum TokenType {
    AUTHENTICATION_TOKEN,
    REFRESH_TOKEN,
    UNDEFINED_TOKEN,
}
