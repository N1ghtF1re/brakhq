package men.brakh.brakhq.localisation

import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.context.support.ResourceBundleMessageSource
import org.springframework.stereotype.Component
import java.util.*


@Component
class Translator (messageSource: ResourceBundleMessageSource) {

    init {
        Translator.messageSource = messageSource
    }


    companion object {
        private var messageSource: ResourceBundleMessageSource? = null


        fun toLocale(msgCode: String): String {
            val locale: Locale = LocaleContextHolder.getLocale()
            return messageSource?.getMessage(msgCode, null, locale) ?: msgCode
        }
    }
}

fun localeString(str: String) = Translator.toLocale(str)