package men.brakh.brakhq.localisation

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.support.ResourceBundleMessageSource
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver
import java.util.*
import javax.servlet.http.HttpServletRequest


@Configuration
open class CustomLocaleResolver : AcceptHeaderLocaleResolver(), WebMvcConfigurer {

    private val LOCALES = listOf(
            Locale("en"),
            Locale("ru")
    )

    override fun resolveLocale(request: HttpServletRequest): Locale? {
        val headerLang: String? = request.getHeader("Accept-Language")

        return if (headerLang == null || headerLang.isEmpty())
            Locale.getDefault()
        else
            Locale.lookup(Locale.LanguageRange.parse(headerLang), LOCALES)
    }

    @Bean
    open fun messageSource(): ResourceBundleMessageSource {
        val rs = ResourceBundleMessageSource()
        rs.setBasename("locales/strings")
        rs.setDefaultEncoding("UTF-8")
        rs.setUseCodeAsDefaultMessage(true)
        return rs
    }
}