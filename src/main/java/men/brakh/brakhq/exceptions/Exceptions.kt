package men.brakh.brakhq.exceptions

import io.jsonwebtoken.ExpiredJwtException
import men.brakh.brakhq.security.auth.token.JwtToken
import org.springframework.security.core.AuthenticationException
import java.util.*

class UnauthorizedException(msg: String): RuntimeException(msg)
class InvalidUserChangingException(val errors: Map<String, String>): RuntimeException(errors.map { it.value }.joinToString(", "))
class InvalidUserRegistrationException(val errors: Map<String, String>): RuntimeException(errors.map { it.value }.joinToString(", "))
class QueueCreationException(msg: String): RuntimeException(msg)
class QueueUpdatingException(msg: String): RuntimeException(msg)
class InvalidJwtTokenException: RuntimeException()
class AttemptToEditAlienQueueException: RuntimeException("Use can't edi't alient queues")
class OAuthException(msg: String) : RuntimeException(msg)

class QueueIsFullException(msg: String): RuntimeException(msg){
    constructor() : this("Queue is full")
}


class NoQueueException(msg: String) : RuntimeException(msg) {
    constructor() : this("Queue not exist")
}

class NoUserException(msg: String) : RuntimeException(msg) {
    constructor() : this("User not exist")
}

class NoPlaceException(msg: String) : RuntimeException(msg) {
    constructor() : this("Place not exist")
}

class StorageException : RuntimeException {
    constructor(message: String) : super(message)
    constructor(message: String, cause: Throwable) : super(message, cause)
}

class StorageFileNotFoundException : RuntimeException {
    constructor(message: String) : super(message)
    constructor(message: String, cause: Throwable) : super(message, cause)
}

class JwtExpiredTokenException(val token: JwtToken, msg: String, t: Throwable) : AuthenticationException(msg) {
    val expirationTime: Date = (t as ExpiredJwtException).claims.expiration
}