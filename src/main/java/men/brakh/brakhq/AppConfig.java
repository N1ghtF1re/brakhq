package men.brakh.brakhq;


import men.brakh.brakhq.model.urlgenerator.ShiftBasedUrlGenerator;
import men.brakh.brakhq.model.urlgenerator.UrlGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    public static final String defaultDateFromat = "yyyy-MM-dd HH:mm:ss.SSS Z";
    public static final String defaultTimeZone = "Europe/Minsk";

    @Bean
    public UrlGenerator urlGenerator() {
        return new ShiftBasedUrlGenerator();
    }
}
